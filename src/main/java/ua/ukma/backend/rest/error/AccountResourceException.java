package ua.ukma.backend.rest.error;

public class AccountResourceException extends RuntimeException {

    public AccountResourceException(String message) {
        super(message);
    }
}
