package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.Album;
import ua.ukma.backend.repository.AlbumRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.AlbumService;
import ua.ukma.backend.service.criteria.AlbumCriteria;
import ua.ukma.backend.service.query.AlbumQueryService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class AlbumController {

    private static final String ENTITY_NAME = "album";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final AlbumService albumService;
    private final AlbumRepository albumRepository;
    private final AlbumQueryService albumQueryService;

    public AlbumController(AlbumService albumService, AlbumRepository albumRepository, AlbumQueryService albumQueryService) {
        this.albumService = albumService;
        this.albumRepository = albumRepository;
        this.albumQueryService = albumQueryService;
    }
    
    @PostMapping("/albums")
    public ResponseEntity<Album> createAlbum(@Valid @RequestBody Album album) throws URISyntaxException {
        log.debug("API call to save Album : {}", album);
        if (album.getId() != null) {
            throw new BadRequestAlertException("A new album cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Album result = albumService.save(album);
        return ResponseEntity
                .created(new URI("/api/albums/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/albums/{id}")
    public ResponseEntity<Album> updateAlbum(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Album album){
        log.debug("API call to update Album : {}, {}", id, album);
        if (album.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, album.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!albumRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Album result = albumService.save(album);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, album.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/albums/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Album> partialUpdateAlbum(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Album album
    ){
        log.debug("API call to partial update Album partially : {}, {}", id, album);
        if (album.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, album.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!albumRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Album> result = albumService.partialUpdate(album);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, album.getId().toString())
        );
    }
    
    @GetMapping("/albums")
    public ResponseEntity<List<Album>> getAllAlbums(AlbumCriteria criteria) {
        log.debug("API call to get Albums by criteria: {}", criteria);
        List<Album> entityList = albumQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /albums/count} : count all the albums.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/albums/count")
    public ResponseEntity<Long> countAlbums(AlbumCriteria criteria) {
        log.debug("API call to count Albums by criteria: {}", criteria);
        return ResponseEntity.ok().body(albumQueryService.countByCriteria(criteria));
    }

    @GetMapping("/albums/{id}")
    public ResponseEntity<Album> getAlbum(@PathVariable Long id) {
        log.debug("API call to get Album : {}", id);
        Optional<Album> album = albumService.findOne(id);
        return ResponseUtil.wrapOrNotFound(album);
    }

    @DeleteMapping("/albums/{id}")
    public ResponseEntity<Void> deleteAlbum(@PathVariable Long id) {
        log.debug("API call to delete Album : {}", id);
        albumService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
