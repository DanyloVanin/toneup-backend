package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ua.ukma.backend.domain.Song;
import ua.ukma.backend.domain.SongRecord;
import ua.ukma.backend.rest.dto.FullSongRecordDTO;
import ua.ukma.backend.service.ChordService;
import ua.ukma.backend.service.SongRecordService;
import ua.ukma.backend.service.SongService;
import ua.ukma.backend.service.criteria.SongRecordCriteria;
import ua.ukma.backend.service.query.SongRecordQueryService;
import ua.ukma.backend.util.PaginationUtil;
import ua.ukma.backend.util.ResponseUtil;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/public")
@Slf4j
public class PublicAccessController {
    
    private final SongRecordService songRecordService;
    private final SongRecordQueryService songRecordQueryService;
    private final SongService songService;
    private final ChordService chordService;
    
    public PublicAccessController(
            SongRecordService songRecordService,
            SongRecordQueryService songRecordQueryService,
            SongService songService, ChordService chordService) {
        this.songRecordService = songRecordService;
        this.songRecordQueryService = songRecordQueryService;
        this.songService = songService;
        this.chordService = chordService;
    }
    
    @GetMapping("/song-records")
    public ResponseEntity<List<SongRecord>> getAllSongRecords(SongRecordCriteria criteria, Pageable pageable) {
        log.debug("Public API call to get SongRecords by criteria: {}", criteria);
        Page<SongRecord> page = songRecordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/song-records/count")
    public ResponseEntity<Long> countSongRecords(SongRecordCriteria criteria) {
        log.debug("Public API call to count SongRecords by criteria: {}", criteria);
        return ResponseEntity.ok().body(songRecordQueryService.countByCriteria(criteria));
    }
    
    @GetMapping("/song-records/{id}")
    public ResponseEntity<FullSongRecordDTO> getSongRecord(@PathVariable Long id) {
        log.debug("Public API call to get SongRecord : {}", id);
        Optional<SongRecord> songRecord = songRecordService.findOne(id);
        if(songRecord.isPresent()){
            Optional<Song> song = songService.findOne(songRecord.get().getSong().getId());
            FullSongRecordDTO result = new FullSongRecordDTO(songRecord.get());
            song.ifPresent(result::setSong);
            return ResponseEntity.ok().body(result);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/songs/{id}")
    public ResponseEntity<Song> getSong(@PathVariable Long id) {
        log.debug("Public API call to get Song : {}", id);
        Optional<Song> song = songService.findOne(id);
        return ResponseUtil.wrapOrNotFound(song);
    }
    
}


