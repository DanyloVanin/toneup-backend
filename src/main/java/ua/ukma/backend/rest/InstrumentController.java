package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.Instrument;
import ua.ukma.backend.repository.InstrumentRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.InstrumentService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class InstrumentController{

    private static final String ENTITY_NAME = "instrument";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final InstrumentService instrumentService;
    private final InstrumentRepository instrumentRepository;

    public InstrumentController(InstrumentService instrumentService, InstrumentRepository instrumentRepository) {
        this.instrumentService = instrumentService;
        this.instrumentRepository = instrumentRepository;
    }
    
    @PostMapping("/instruments")
    public ResponseEntity<Instrument> createInstrument(@Valid @RequestBody Instrument instrument) throws URISyntaxException {
        log.debug("API call to save Instrument : {}", instrument);
        if (instrument.getId() != null) {
            throw new BadRequestAlertException("A new instrument cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Instrument result = instrumentService.save(instrument);
        return ResponseEntity
                .created(new URI("/api/instruments/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/instruments/{id}")
    public ResponseEntity<Instrument> updateInstrument(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody Instrument instrument
    ) throws URISyntaxException {
        log.debug("API call to update Instrument : {}, {}", id, instrument);
        if (instrument.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, instrument.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!instrumentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Instrument result = instrumentService.save(instrument);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, instrument.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/instruments/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Instrument> partialUpdateInstrument(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Instrument instrument
    ) throws URISyntaxException {
        log.debug("API call to partial update Instrument partially : {}, {}", id, instrument);
        if (instrument.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, instrument.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!instrumentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Instrument> result = instrumentService.partialUpdate(instrument);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, instrument.getId().toString())
        );
    }
    
    @GetMapping("/instruments")
    public List<Instrument> getAllInstruments(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("API call to get all Instruments");
        return instrumentService.findAll();
    }
    
    @GetMapping("/instruments/{id}")
    public ResponseEntity<Instrument> getInstrument(@PathVariable Long id) {
        log.debug("API call to get Instrument : {}", id);
        Optional<Instrument> instrument = instrumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(instrument);
    }

    @DeleteMapping("/instruments/{id}")
    public ResponseEntity<Void> deleteInstrument(@PathVariable Long id) {
        log.debug("API call to delete Instrument : {}", id);
        instrumentService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}

