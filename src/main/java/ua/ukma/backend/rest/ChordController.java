package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.Chord;
import ua.ukma.backend.repository.ChordRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.ChordService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class ChordController{

    private static final String ENTITY_NAME = "chord";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final ChordService chordService;
    private final ChordRepository chordRepository;

    public ChordController(ChordService chordService, ChordRepository chordRepository) {
        this.chordService = chordService;
        this.chordRepository = chordRepository;
    }
    
    @PostMapping("/chords")
    public ResponseEntity<Chord> createChord(@Valid @RequestBody Chord chord) throws URISyntaxException {
        log.debug("API call to save Chord : {}", chord);
        if (chord.getId() != null) {
            throw new BadRequestAlertException("A new chord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Chord result = chordService.save(chord);
        return ResponseEntity
                .created(new URI("/api/chords/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/chords/{id}")
    public ResponseEntity<Chord> updateChord(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Chord chord){
        log.debug("API call to update Chord : {}, {}", id, chord);
        if (chord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Chord result = chordService.save(chord);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chord.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/chords/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Chord> partialUpdateChord(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Chord chord
    ){
        log.debug("API call to partial update Chord partially : {}, {}", id, chord);
        if (chord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Chord> result = chordService.partialUpdate(chord);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chord.getId().toString())
        );
    }
    
    @GetMapping("/chords")
    public List<Chord> getAllChords() {
        log.debug("API call to get all Chords");
        return chordService.findAll();
    }
    
    @GetMapping("/chords/{id}")
    public ResponseEntity<Chord> getChord(@PathVariable Long id) {
        log.debug("API call to get Chord : {}", id);
        Optional<Chord> chord = chordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chord);
    }
    
    @DeleteMapping("/chords/{id}")
    public ResponseEntity<Void> deleteChord(@PathVariable Long id) {
        log.debug("API call to delete Chord : {}", id);
        chordService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}

