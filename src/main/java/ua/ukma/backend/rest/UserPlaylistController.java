package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.UserPlaylist;
import ua.ukma.backend.repository.UserPlaylistRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.UserPlaylistService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class UserPlaylistController{

    private static final String ENTITY_NAME = "userPlaylist";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final UserPlaylistService userPlaylistService;
    private final UserPlaylistRepository userPlaylistRepository;

    public UserPlaylistController(UserPlaylistService userPlaylistService, UserPlaylistRepository userPlaylistRepository) {
        this.userPlaylistService = userPlaylistService;
        this.userPlaylistRepository = userPlaylistRepository;
    }
    
    @PostMapping("/user-playlists")
    public ResponseEntity<UserPlaylist> createUserPlaylist(@Valid @RequestBody UserPlaylist userPlaylist) throws URISyntaxException {
        log.debug("API call to save UserPlaylist : {}", userPlaylist);
        if (userPlaylist.getId() != null) {
            throw new BadRequestAlertException("A new userPlaylist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserPlaylist result = userPlaylistService.save(userPlaylist);
        return ResponseEntity
                .created(new URI("/api/user-playlists/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/user-playlists/{id}")
    public ResponseEntity<UserPlaylist> updateUserPlaylist(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody UserPlaylist userPlaylist
    ) throws URISyntaxException {
        log.debug("API call to update UserPlaylist : {}, {}", id, userPlaylist);
        if (userPlaylist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userPlaylist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userPlaylistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserPlaylist result = userPlaylistService.save(userPlaylist);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userPlaylist.getId().toString()))
                .body(result);
    }

    @PatchMapping(value = "/user-playlists/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<UserPlaylist> partialUpdateUserPlaylist(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody UserPlaylist userPlaylist
    ) throws URISyntaxException {
        log.debug("API call to partial update UserPlaylist partially : {}, {}", id, userPlaylist);
        if (userPlaylist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userPlaylist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userPlaylistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserPlaylist> result = userPlaylistService.partialUpdate(userPlaylist);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userPlaylist.getId().toString())
        );
    }
    
    @GetMapping("/user-playlists")
    public List<UserPlaylist> getAllUserPlaylists() {
        log.debug("API call to get all UserPlaylists");
        return userPlaylistService.findAll();
    }

    @GetMapping("/user-playlists/{id}")
    public ResponseEntity<UserPlaylist> getUserPlaylist(@PathVariable Long id) {
        log.debug("API call to get UserPlaylist : {}", id);
        Optional<UserPlaylist> userPlaylist = userPlaylistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userPlaylist);
    }
    
    @DeleteMapping("/user-playlists/{id}")
    public ResponseEntity<Void> deleteUserPlaylist(@PathVariable Long id) {
        log.debug("API call to delete UserPlaylist : {}", id);
        userPlaylistService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}

