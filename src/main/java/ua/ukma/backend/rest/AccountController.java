package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.config.GeneralConfig;
import ua.ukma.backend.domain.User;
import ua.ukma.backend.repository.UserRepository;
import ua.ukma.backend.rest.dto.KeyPasswordDTO;
import ua.ukma.backend.rest.dto.ManagedUserDTO;
import ua.ukma.backend.rest.error.AccountResourceException;
import ua.ukma.backend.security.SecurityUtils;
import ua.ukma.backend.service.UserService;
import ua.ukma.backend.service.dto.AdminUserDTO;
import ua.ukma.backend.service.dto.PasswordChangeDTO;
import ua.ukma.backend.service.exception.EmailAlreadyUsedException;
import ua.ukma.backend.service.exception.InvalidPasswordException;
import ua.ukma.backend.service.mail.MailService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
 
@RestController
@RequestMapping("/api")
@Slf4j
public class AccountController{
    
    private final UserRepository userRepository;
    private final UserService userService;
    private final MailService mailService;
    private final GeneralConfig config;

    public AccountController(UserRepository userRepository, UserService userService, MailService mailService, GeneralConfig config){
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.config = config;
    }
    
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserDTO managedUserDTO) {
        if (isPasswordLengthInvalid(managedUserDTO.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.registerUser(managedUserDTO, managedUserDTO.getPassword());
        if (config.getValidateMail())
            mailService.sendActivationEmail(user);
    }
    
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("API call to check if the current user is authenticated");
        return request.getRemoteUser();
    }
    
    @GetMapping("/account")
    public AdminUserDTO getAccount() {
        return userService
                .getUserWithAuthorities()
                .map(AdminUserDTO::new)
                .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }
    
    @PostMapping("/account")
    public void saveAccount(@Valid @RequestBody AdminUserDTO userDTO) {
        String userLogin = SecurityUtils
                .getCurrentUserLogin()
                .orElseThrow(() -> new AccountResourceException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (user.isEmpty()) {
            throw new AccountResourceException("User could not be found");
        }
        userService.updateUser(
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getEmail(),
                userDTO.getLangKey(),
                userDTO.getImageUrl()
        );
    }
    
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (isPasswordLengthInvalid(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }
    
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
        Optional<User> user = userService.requestPasswordReset(mail);
        if (user.isPresent()) {
            if(config.getValidateMail())
                mailService.sendPasswordResetMail(user.get());
            else log.warn("Password reset not implemented");
        } else {
            log.warn("Password reset requested for non existing mail");
        }
    }
    
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody KeyPasswordDTO keyPasswordDTO) {
        if (isPasswordLengthInvalid(keyPasswordDTO.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user = userService.completePasswordReset(keyPasswordDTO.getNewPassword(), keyPasswordDTO.getKey());
        if (user.isEmpty()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
    }

    private static boolean isPasswordLengthInvalid(String password) {
        return (
                StringUtils.isEmpty(password) ||
                        password.length() < ManagedUserDTO.PASSWORD_MIN_LENGTH ||
                        password.length() > ManagedUserDTO.PASSWORD_MAX_LENGTH
        );
    }
}
