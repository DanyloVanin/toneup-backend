package ua.ukma.backend.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ua.ukma.backend.domain.Article;
import ua.ukma.backend.repository.ArticleRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.ArticleService;
import ua.ukma.backend.service.criteria.ArticleCriteria;
import ua.ukma.backend.service.query.ArticleQueryService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.PaginationUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ArticleController{

    private final Logger log = LoggerFactory.getLogger(ArticleController.class);

    private static final String ENTITY_NAME = "article";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final ArticleService articleService;
    private final ArticleRepository articleRepository;
    private final ArticleQueryService articleQueryService;

    public ArticleController(ArticleService articleService, ArticleRepository articleRepository, ArticleQueryService articleQueryService) {
        this.articleService = articleService;
        this.articleRepository = articleRepository;
        this.articleQueryService = articleQueryService;
    }
    
    @PostMapping("/articles")
    public ResponseEntity<Article> createArticle(@Valid @RequestBody Article article) throws URISyntaxException {
        log.debug("API call to save Article : {}", article);
        if (article.getId() != null) {
            throw new BadRequestAlertException("A new article cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Article result = articleService.save(article);
        return ResponseEntity
                .created(new URI("/api/articles/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/articles/{id}")
    public ResponseEntity<Article> updateArticle(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody Article article
    ){
        log.debug("API call to update Article : {}, {}", id, article);
        if (article.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, article.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!articleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Article result = articleService.save(article);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, article.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/articles/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Article> partialUpdateArticle(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Article article
    ){
        log.debug("API call to partial update Article partially : {}, {}", id, article);
        if (article.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, article.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!articleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Article> result = articleService.partialUpdate(article);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, article.getId().toString())
        );
    }

    @GetMapping("/articles")
    public ResponseEntity<List<Article>> getAllArticles(ArticleCriteria criteria, Pageable pageable) {
        log.debug("API call to get Articles by criteria: {}", criteria);
        Page<Article> page = articleQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/articles/count")
    public ResponseEntity<Long> countArticles(ArticleCriteria criteria) {
        log.debug("API call to count Articles by criteria: {}", criteria);
        return ResponseEntity.ok().body(articleQueryService.countByCriteria(criteria));
    }
    
    @GetMapping("/articles/{id}")
    public ResponseEntity<Article> getArticle(@PathVariable Long id) {
        log.debug("API call to get Article : {}", id);
        Optional<Article> article = articleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(article);
    }
    
    @DeleteMapping("/articles/{id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable Long id) {
        log.debug("API call to delete Article : {}", id);
        articleService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
