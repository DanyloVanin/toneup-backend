package ua.ukma.backend.rest.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static ua.ukma.backend.rest.dto.ManagedUserDTO.PASSWORD_MAX_LENGTH;
import static ua.ukma.backend.rest.dto.ManagedUserDTO.PASSWORD_MIN_LENGTH;

@Data
public class LoginDTO {

    @NotNull
    @Size(min = 1, max = 50)
    private String username;

    @NotNull
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    private boolean rememberMe;
    
}
