package ua.ukma.backend.rest.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ukma.backend.service.dto.AdminUserDTO;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class ManagedUserDTO extends AdminUserDTO{

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;
    
}
