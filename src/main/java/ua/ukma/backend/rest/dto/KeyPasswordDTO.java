package ua.ukma.backend.rest.dto;

import lombok.Data;

@Data
public class KeyPasswordDTO {

    private String key;
    private String newPassword;
    
}
