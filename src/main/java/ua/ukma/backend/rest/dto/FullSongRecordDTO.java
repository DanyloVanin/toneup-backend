package ua.ukma.backend.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import ua.ukma.backend.domain.*;
import ua.ukma.backend.domain.enumeration.Difficulty;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

@Data
public class FullSongRecordDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String tonality;
    private String lyrics;
    private Integer tempo;
    private Float rating;
    private Instant dateUploaded;
    private Difficulty difficulty;
    @JsonIgnoreProperties(value = { "userPlaylists", "userComments", "userArticles",
            "email", "firstName", "lastName", "langKey", "resetDate", "sex", "activated"
    }, allowSetters = true)
    private User author;
    @JsonIgnoreProperties(value = { "song" }, allowSetters = true)
    private Set<Comment> comments;
    @JsonIgnoreProperties(value = { "prevChord", "nextChord", "songRecords" }, allowSetters = true)
    private Set<Chord> chords;
    @JsonIgnoreProperties(value = { "songRecords"}, allowSetters = true)
    private Song song;
    
    public FullSongRecordDTO(SongRecord songRecord) {
        this.id = songRecord.getId();
        this.tonality= songRecord.getTonality();
        this.lyrics = songRecord.getLyrics();
        this.tempo = songRecord.getTempo();
        this.rating = songRecord.getRating();
        this.dateUploaded = songRecord.getDateUploaded();
        this. difficulty = songRecord.getDifficulty();
        this. author = songRecord.getAuthor();
        this.comments = songRecord.getComments();
        this.chords = songRecord.getChords();
        this.song = songRecord.getSong();
    }
}
