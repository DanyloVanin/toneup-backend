package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.ChordPic;
import ua.ukma.backend.repository.ChordPicRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.ChordPicService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class ChordPicController{

    private static final String ENTITY_NAME = "chordPic";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final ChordPicService chordPicService;
    private final ChordPicRepository chordPicRepository;

    public ChordPicController(ChordPicService chordPicService, ChordPicRepository chordPicRepository) {
        this.chordPicService = chordPicService;
        this.chordPicRepository = chordPicRepository;
    }
    
    @PostMapping("/chord-pics")
    public ResponseEntity<ChordPic> createChordPic(@Valid @RequestBody ChordPic chordPic) throws URISyntaxException {
        log.debug("API call to save ChordPic : {}", chordPic);
        if (chordPic.getId() != null) {
            throw new BadRequestAlertException("A new chordPic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChordPic result = chordPicService.save(chordPic);
        return ResponseEntity
                .created(new URI("/api/chord-pics/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/chord-pics/{id}")
    public ResponseEntity<ChordPic> updateChordPic(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody ChordPic chordPic
    ) throws URISyntaxException {
        log.debug("API call to update ChordPic : {}, {}", id, chordPic);
        if (chordPic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chordPic.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chordPicRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChordPic result = chordPicService.save(chordPic);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chordPic.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/chord-pics/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ChordPic> partialUpdateChordPic(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody ChordPic chordPic
    ) throws URISyntaxException {
        log.debug("API call to partial update ChordPic partially : {}, {}", id, chordPic);
        if (chordPic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, chordPic.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!chordPicRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChordPic> result = chordPicService.partialUpdate(chordPic);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, chordPic.getId().toString())
        );
    }
    
    @GetMapping("/chord-pics")
    public List<ChordPic> getAllChordPics() {
        log.debug("API call to get all ChordPics");
        return chordPicService.findAll();
    }
    
    @GetMapping("/chord-pics/{id}")
    public ResponseEntity<ChordPic> getChordPic(@PathVariable Long id) {
        log.debug("API call to get ChordPic : {}", id);
        Optional<ChordPic> chordPic = chordPicService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chordPic);
    }
    
    @DeleteMapping("/chord-pics/{id}")
    public ResponseEntity<Void> deleteChordPic(@PathVariable Long id) {
        log.debug("API call to delete ChordPic : {}", id);
        chordPicService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
