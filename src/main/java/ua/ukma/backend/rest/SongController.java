package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ua.ukma.backend.domain.Song;
import ua.ukma.backend.repository.SongRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.SongService;
import ua.ukma.backend.service.criteria.SongCriteria;
import ua.ukma.backend.service.query.SongQueryService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.PaginationUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class SongController{

    private static final String ENTITY_NAME = "song";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final SongService songService;
    private final SongRepository songRepository;
    private final SongQueryService songQueryService;

    public SongController(SongService songService, SongRepository songRepository, SongQueryService songQueryService) {
        this.songService = songService;
        this.songRepository = songRepository;
        this.songQueryService = songQueryService;
    }
    
    @PostMapping("/songs")
    public ResponseEntity<Song> createSong(@Valid @RequestBody Song song) throws URISyntaxException {
        log.debug("API call to save Song : {}", song);
        if (song.getId() != null) {
            throw new BadRequestAlertException("A new song cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Song result = songService.save(song);
        return ResponseEntity
                .created(new URI("/api/songs/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/songs/{id}")
    public ResponseEntity<Song> updateSong(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Song song)
            throws URISyntaxException {
        log.debug("API call to update Song : {}, {}", id, song);
        if (song.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, song.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Song result = songService.save(song);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, song.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/songs/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Song> partialUpdateSong(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Song song
    ) throws URISyntaxException {
        log.debug("API call to partial update Song partially : {}, {}", id, song);
        if (song.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, song.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Song> result = songService.partialUpdate(song);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, song.getId().toString())
        );
    }
    
    @GetMapping("/songs")
    public ResponseEntity<List<Song>> getAllSongs(SongCriteria criteria, Pageable pageable) {
        log.debug("API call to get Songs by criteria: {}", criteria);
        Page<Song> page = songQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/songs/count")
    public ResponseEntity<Long> countSongs(SongCriteria criteria) {
        log.debug("API call to count Songs by criteria: {}", criteria);
        return ResponseEntity.ok().body(songQueryService.countByCriteria(criteria));
    }
    
    @GetMapping("/songs/{id}")
    public ResponseEntity<Song> getSong(@PathVariable Long id) {
        log.debug("API call to get Song : {}", id);
        Optional<Song> song = songService.findOne(id);
        return ResponseUtil.wrapOrNotFound(song);
    }
    
    @DeleteMapping("/songs/{id}")
    public ResponseEntity<Void> deleteSong(@PathVariable Long id) {
        log.debug("API call to delete Song : {}", id);
        songService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
