package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ua.ukma.backend.domain.SongRecord;
import ua.ukma.backend.repository.SongRecordRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.SongRecordService;
import ua.ukma.backend.service.criteria.SongRecordCriteria;
import ua.ukma.backend.service.query.SongRecordQueryService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.PaginationUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class SongRecordController{

    private static final String ENTITY_NAME = "songRecord";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final SongRecordService songRecordService;
    private final SongRecordRepository songRecordRepository;
    private final SongRecordQueryService songRecordQueryService;

    public SongRecordController(
            SongRecordService songRecordService,
            SongRecordRepository songRecordRepository,
            SongRecordQueryService songRecordQueryService
    ) {
        this.songRecordService = songRecordService;
        this.songRecordRepository = songRecordRepository;
        this.songRecordQueryService = songRecordQueryService;
    }
    
    @PostMapping("/song-records")
    public ResponseEntity<SongRecord> createSongRecord(@Valid @RequestBody SongRecord songRecord) throws URISyntaxException {
        log.debug("API call to save SongRecord : {}", songRecord);
        if (songRecord.getId() != null) {
            throw new BadRequestAlertException("A new songRecord cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SongRecord result = songRecordService.save(songRecord);
        return ResponseEntity
                .created(new URI("/api/song-records/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @PutMapping("/song-records/{id}")
    public ResponseEntity<SongRecord> updateSongRecord(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody SongRecord songRecord
    ) throws URISyntaxException {
        log.debug("API call to update SongRecord : {}, {}", id, songRecord);
        if (songRecord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, songRecord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songRecordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SongRecord result = songRecordService.save(songRecord);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, songRecord.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/song-records/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<SongRecord> partialUpdateSongRecord(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody SongRecord songRecord
    ) throws URISyntaxException {
        log.debug("API call to partial update SongRecord partially : {}, {}", id, songRecord);
        if (songRecord.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, songRecord.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songRecordRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SongRecord> result = songRecordService.partialUpdate(songRecord);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, songRecord.getId().toString())
        );
    }
    
    @GetMapping("/song-records")
    public ResponseEntity<List<SongRecord>> getAllSongRecords(SongRecordCriteria criteria, Pageable pageable) {
        log.debug("API call to get SongRecords by criteria: {}", criteria);
        Page<SongRecord> page = songRecordQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/song-records/count")
    public ResponseEntity<Long> countSongRecords(SongRecordCriteria criteria) {
        log.debug("API call to count SongRecords by criteria: {}", criteria);
        return ResponseEntity.ok().body(songRecordQueryService.countByCriteria(criteria));
    }
    
    @GetMapping("/song-records/{id}")
    public ResponseEntity<SongRecord> getSongRecord(@PathVariable Long id) {
        log.debug("API call to get SongRecord : {}", id);
        Optional<SongRecord> songRecord = songRecordService.findOne(id);
        return ResponseUtil.wrapOrNotFound(songRecord);
    }
    
    @DeleteMapping("/song-records/{id}")
    public ResponseEntity<Void> deleteSongRecord(@PathVariable Long id) {
        log.debug("API call to delete SongRecord : {}", id);
        songRecordService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}

