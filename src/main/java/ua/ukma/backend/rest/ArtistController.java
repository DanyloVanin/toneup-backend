package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.Artist;
import ua.ukma.backend.repository.ArtistRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.ArtistService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class ArtistController{

    private static final String ENTITY_NAME = "artist";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final ArtistService artistService;

    private final ArtistRepository artistRepository;

    public ArtistController(ArtistService artistService, ArtistRepository artistRepository) {
        this.artistService = artistService;
        this.artistRepository = artistRepository;
    }
    
    @PostMapping("/artists")
    public ResponseEntity<Artist> createArtist(@Valid @RequestBody Artist artist) throws URISyntaxException {
        log.debug("API call to save Artist : {}", artist);
        if (artist.getId() != null) {
            throw new BadRequestAlertException("A new artist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Artist result = artistService.save(artist);
        return ResponseEntity
                .created(new URI("/api/artists/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/artists/{id}")
    public ResponseEntity<Artist> updateArtist(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody Artist artist
    ) throws URISyntaxException {
        log.debug("API call to update Artist : {}, {}", id, artist);
        if (artist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, artist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!artistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Artist result = artistService.save(artist);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, artist.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/artists/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Artist> partialUpdateArtist(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody Artist artist
    ) throws URISyntaxException {
        log.debug("API call to partial update Artist partially : {}, {}", id, artist);
        if (artist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, artist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!artistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Artist> result = artistService.partialUpdate(artist);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, artist.getId().toString())
        );
    }
    
    @GetMapping("/artists")
    public List<Artist> getAllArtists(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("API call to get all Artists");
        return artistService.findAll();
    }
    
    @GetMapping("/artists/{id}")
    public ResponseEntity<Artist> getArtist(@PathVariable Long id) {
        log.debug("API call to get Artist : {}", id);
        Optional<Artist> artist = artistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(artist);
    }
    
    @DeleteMapping("/artists/{id}")
    public ResponseEntity<Void> deleteArtist(@PathVariable Long id) {
        log.debug("API call to delete Artist : {}", id);
        artistService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}
