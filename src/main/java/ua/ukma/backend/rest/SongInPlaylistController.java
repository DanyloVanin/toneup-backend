package ua.ukma.backend.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ukma.backend.domain.SongInPlaylist;
import ua.ukma.backend.repository.SongInPlaylistRepository;
import ua.ukma.backend.rest.error.BadRequestAlertException;
import ua.ukma.backend.service.SongInPlaylistService;
import ua.ukma.backend.service.criteria.SongInPlaylistCriteria;
import ua.ukma.backend.service.query.SongInPlaylistQueryService;
import ua.ukma.backend.util.HeaderUtil;
import ua.ukma.backend.util.ResponseUtil;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class SongInPlaylistController{

    private static final String ENTITY_NAME = "songInPlaylist";

    @Value("${general.clientApp.name}")
    private String applicationName;

    private final SongInPlaylistService songInPlaylistService;
    private final SongInPlaylistRepository songInPlaylistRepository;
    private final SongInPlaylistQueryService songInPlaylistQueryService;

    public SongInPlaylistController(
            SongInPlaylistService songInPlaylistService,
            SongInPlaylistRepository songInPlaylistRepository,
            SongInPlaylistQueryService songInPlaylistQueryService
    ) {
        this.songInPlaylistService = songInPlaylistService;
        this.songInPlaylistRepository = songInPlaylistRepository;
        this.songInPlaylistQueryService = songInPlaylistQueryService;
    }
    
    @PostMapping("/song-in-playlists")
    public ResponseEntity<SongInPlaylist> createSongInPlaylist(@Valid @RequestBody SongInPlaylist songInPlaylist)
            throws URISyntaxException {
        log.debug("API call to save SongInPlaylist : {}", songInPlaylist);
        if (songInPlaylist.getId() != null) {
            throw new BadRequestAlertException("A new songInPlaylist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SongInPlaylist result = songInPlaylistService.save(songInPlaylist);
        return ResponseEntity
                .created(new URI("/api/song-in-playlists/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
    
    @PutMapping("/song-in-playlists/{id}")
    public ResponseEntity<SongInPlaylist> updateSongInPlaylist(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody SongInPlaylist songInPlaylist
    ) throws URISyntaxException {
        log.debug("API call to update SongInPlaylist : {}, {}", id, songInPlaylist);
        if (songInPlaylist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, songInPlaylist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songInPlaylistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SongInPlaylist result = songInPlaylistService.save(songInPlaylist);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, songInPlaylist.getId().toString()))
                .body(result);
    }
    
    @PatchMapping(value = "/song-in-playlists/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<SongInPlaylist> partialUpdateSongInPlaylist(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody SongInPlaylist songInPlaylist
    ) throws URISyntaxException {
        log.debug("API call to partial update SongInPlaylist partially : {}, {}", id, songInPlaylist);
        if (songInPlaylist.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, songInPlaylist.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!songInPlaylistRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SongInPlaylist> result = songInPlaylistService.partialUpdate(songInPlaylist);

        return ResponseUtil.wrapOrNotFound(
                result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, songInPlaylist.getId().toString())
        );
    }
    
    @GetMapping("/song-in-playlists")
    public ResponseEntity<List<SongInPlaylist>> getAllSongInPlaylists(SongInPlaylistCriteria criteria) {
        log.debug("API call to get SongInPlaylists by criteria: {}", criteria);
        List<SongInPlaylist> entityList = songInPlaylistQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }
    
    @GetMapping("/song-in-playlists/count")
    public ResponseEntity<Long> countSongInPlaylists(SongInPlaylistCriteria criteria) {
        log.debug("API call to count SongInPlaylists by criteria: {}", criteria);
        return ResponseEntity.ok().body(songInPlaylistQueryService.countByCriteria(criteria));
    }
    
    @GetMapping("/song-in-playlists/{id}")
    public ResponseEntity<SongInPlaylist> getSongInPlaylist(@PathVariable Long id) {
        log.debug("API call to get SongInPlaylist : {}", id);
        Optional<SongInPlaylist> songInPlaylist = songInPlaylistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(songInPlaylist);
    }
    
    @DeleteMapping("/song-in-playlists/{id}")
    public ResponseEntity<Void> deleteSongInPlaylist(@PathVariable Long id) {
        log.debug("API call to delete SongInPlaylist : {}", id);
        songInPlaylistService.delete(id);
        return ResponseEntity
                .noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }
}