package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Instrument;
import ua.ukma.backend.repository.InstrumentRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class InstrumentService {

    private final InstrumentRepository instrumentRepository;

    public InstrumentService(InstrumentRepository instrumentRepository) {
        this.instrumentRepository = instrumentRepository;
    }

    public Instrument save(Instrument instrument) {
        log.debug("Request to save Instrument : {}", instrument);
        return instrumentRepository.save(instrument);
    }
    
    public Optional<Instrument> partialUpdate(Instrument instrument) {
        log.debug("Request to partially update Instrument : {}", instrument);

        return instrumentRepository
                .findById(instrument.getId())
                .map(
                        existingInstrument -> {
                            if (instrument.getName() != null) {
                                existingInstrument.setName(instrument.getName());
                            }

                            return existingInstrument;
                        }
                )
                .map(instrumentRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Instrument> findAll() {
        log.debug("Request to get all Instruments");
        return instrumentRepository.findAllWithEagerRelationships();
    }
    
    public Page<Instrument> findAllWithEagerRelationships(Pageable pageable) {
        return instrumentRepository.findAllWithEagerRelationships(pageable);
    }
    
    @Transactional(readOnly = true)
    public Optional<Instrument> findOne(Long id) {
        log.debug("Request to get Instrument : {}", id);
        return instrumentRepository.findOneWithEagerRelationships(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete Instrument : {}", id);
        instrumentRepository.deleteById(id);
    }
}