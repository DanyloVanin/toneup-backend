package ua.ukma.backend.service.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Album;
import ua.ukma.backend.domain.Album_;
import ua.ukma.backend.domain.Artist_;
import ua.ukma.backend.domain.Genre_;
import ua.ukma.backend.repository.AlbumRepository;
import ua.ukma.backend.service.criteria.AlbumCriteria;

import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
@Transactional(readOnly = true)
@Slf4j
public class AlbumQueryService extends QueryService<Album> {

    private final AlbumRepository albumRepository;

    public AlbumQueryService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }
    
    @Transactional(readOnly = true)
    public List<Album> findByCriteria(AlbumCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Album> specification = createSpecification(criteria);
        return albumRepository.findAll(specification);
    }
    
    @Transactional(readOnly = true)
    public Page<Album> findByCriteria(AlbumCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Album> specification = createSpecification(criteria);
        return albumRepository.findAll(specification, page);
    }
    
    @Transactional(readOnly = true)
    public long countByCriteria(AlbumCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Album> specification = createSpecification(criteria);
        return albumRepository.count(specification);
    }
    
    protected Specification<Album> createSpecification(AlbumCriteria criteria) {
        Specification<Album> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Album_.id));
            }
            if (criteria.getAlbumName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAlbumName(), Album_.albumName));
            }
            if (criteria.getReleaseYear() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReleaseYear(), Album_.releaseYear));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Album_.description));
            }
            if (criteria.getAlbumPicUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAlbumPicUrl(), Album_.albumPicUrl));
            }
            if (criteria.getGenreId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getGenreId(), root -> root.join(Album_.genres, JoinType.LEFT).get(Genre_.id))
                        );
            }
            if (criteria.getArtistId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getArtistId(), root -> root.join(Album_.artists, JoinType.LEFT).get(Artist_.id))
                        );
            }
        }
        return specification;
    }
}
