package ua.ukma.backend.service.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.*;
import ua.ukma.backend.repository.SongRecordRepository;
import ua.ukma.backend.service.criteria.SongRecordCriteria;

import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
@Transactional(readOnly = true)
@Slf4j
public class SongRecordQueryService extends QueryService<SongRecord> {

    private final SongRecordRepository songRecordRepository;

    public SongRecordQueryService(SongRecordRepository songRecordRepository) {
        this.songRecordRepository = songRecordRepository;
    }
    
    @Transactional(readOnly = true)
    public List<SongRecord> findByCriteria(SongRecordCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SongRecord> specification = createSpecification(criteria);
        return songRecordRepository.findAll(specification);
    }
    
    @Transactional(readOnly = true)
    public Page<SongRecord> findByCriteria(SongRecordCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SongRecord> specification = createSpecification(criteria);
        return songRecordRepository.findAll(specification, page);
    }
    
    @Transactional(readOnly = true)
    public long countByCriteria(SongRecordCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SongRecord> specification = createSpecification(criteria);
        return songRecordRepository.count(specification);
    }
    
    protected Specification<SongRecord> createSpecification(SongRecordCriteria criteria) {
        Specification<SongRecord> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SongRecord_.id));
            }
            if (criteria.getTonality() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTonality(), SongRecord_.tonality));
            }
            if (criteria.getTempo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTempo(), SongRecord_.tempo));
            }
            if (criteria.getRating() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRating(), SongRecord_.rating));
            }
            if (criteria.getDateUploaded() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateUploaded(), SongRecord_.dateUploaded));
            }
            if (criteria.getDifficulty() != null) {
                specification = specification.and(buildSpecification(criteria.getDifficulty(), SongRecord_.difficulty));
            }
            if (criteria.getAuthorId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getAuthorId(), root -> root.join(SongRecord_.author, JoinType.LEFT).get(User_.id))
                        );
            }
            if (criteria.getSongInPlaylistId() != null) {
                specification =
                        specification.and(
                                buildSpecification(
                                        criteria.getSongInPlaylistId(),
                                        root -> root.join(SongRecord_.songInPlaylists, JoinType.LEFT).get(SongInPlaylist_.id)
                                )
                        );
            }
            if (criteria.getCommentId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getCommentId(), root -> root.join(SongRecord_.comments, JoinType.LEFT).get(Comment_.id))
                        );
            }
            if (criteria.getChordId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getChordId(), root -> root.join(SongRecord_.chords, JoinType.LEFT).get(Chord_.id))
                        );
            }
            if (criteria.getSongId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getSongId(), root -> root.join(SongRecord_.song, JoinType.LEFT).get(Song_.id))
                        );
            }
        }
        return specification;
    }
}
