package ua.ukma.backend.service.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.SongInPlaylist;
import ua.ukma.backend.domain.SongInPlaylist_;
import ua.ukma.backend.domain.SongRecord_;
import ua.ukma.backend.domain.UserPlaylist_;
import ua.ukma.backend.repository.SongInPlaylistRepository;
import ua.ukma.backend.service.criteria.SongInPlaylistCriteria;

import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
@Transactional(readOnly = true)
@Slf4j
public class SongInPlaylistQueryService extends QueryService<SongInPlaylist> {

    private final SongInPlaylistRepository songInPlaylistRepository;

    public SongInPlaylistQueryService(SongInPlaylistRepository songInPlaylistRepository) {
        this.songInPlaylistRepository = songInPlaylistRepository;
    }
    
    @Transactional(readOnly = true)
    public List<SongInPlaylist> findByCriteria(SongInPlaylistCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SongInPlaylist> specification = createSpecification(criteria);
        return songInPlaylistRepository.findAll(specification);
    }
    
    @Transactional(readOnly = true)
    public Page<SongInPlaylist> findByCriteria(SongInPlaylistCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SongInPlaylist> specification = createSpecification(criteria);
        return songInPlaylistRepository.findAll(specification, page);
    }
    
    @Transactional(readOnly = true)
    public long countByCriteria(SongInPlaylistCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SongInPlaylist> specification = createSpecification(criteria);
        return songInPlaylistRepository.count(specification);
    }
    
    protected Specification<SongInPlaylist> createSpecification(SongInPlaylistCriteria criteria) {
        Specification<SongInPlaylist> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), SongInPlaylist_.id));
            }
            if (criteria.getDateAdded() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateAdded(), SongInPlaylist_.dateAdded));
            }
            if (criteria.getSondRecordId() != null) {
                specification =
                        specification.and(
                                buildSpecification(
                                        criteria.getSondRecordId(),
                                        root -> root.join(SongInPlaylist_.songRecord, JoinType.LEFT).get(SongRecord_.id)
                                )
                        );
            }
            if (criteria.getUserPlaylistIdId() != null) {
                specification =
                        specification.and(
                                buildSpecification(
                                        criteria.getUserPlaylistIdId(),
                                        root -> root.join(SongInPlaylist_.userPlaylist, JoinType.LEFT).get(UserPlaylist_.id)
                                )
                        );
            }
        }
        return specification;
    }
}
