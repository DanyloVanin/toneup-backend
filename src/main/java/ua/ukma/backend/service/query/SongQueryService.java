package ua.ukma.backend.service.query;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.*;
import ua.ukma.backend.repository.SongRepository;
import ua.ukma.backend.service.criteria.SongCriteria;

import javax.persistence.criteria.JoinType;
import java.util.List;

@Service
@Transactional(readOnly = true)
@Slf4j
public class SongQueryService extends QueryService<Song> {

    private final SongRepository songRepository;

    public SongQueryService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }
    
    @Transactional(readOnly = true)
    public List<Song> findByCriteria(SongCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Song> specification = createSpecification(criteria);
        return songRepository.findAll(specification);
    }
    
    @Transactional(readOnly = true)
    public Page<Song> findByCriteria(SongCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Song> specification = createSpecification(criteria);
        return songRepository.findAll(specification, page);
    }
    
    @Transactional(readOnly = true)
    public long countByCriteria(SongCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Song> specification = createSpecification(criteria);
        return songRepository.count(specification);
    }
    
    protected Specification<Song> createSpecification(SongCriteria criteria) {
        Specification<Song> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Song_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Song_.name));
            }
            if (criteria.getLanguage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLanguage(), Song_.language));
            }
            if (criteria.getDateAdded() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateAdded(), Song_.dateAdded));
            }
            if (criteria.getSpotifyUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpotifyUrl(), Song_.spotifyUrl));
            }
            if (criteria.getYoutubeUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getYoutubeUrl(), Song_.youtubeUrl));
            }
            if (criteria.getSongRecordId() != null) {
                specification =
                        specification.and(
                                buildSpecification(
                                        criteria.getSongRecordId(),
                                        root -> root.join(Song_.songRecords, JoinType.LEFT).get(SongRecord_.id)
                                )
                        );
            }
            if (criteria.getGenreId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getGenreId(), root -> root.join(Song_.genres, JoinType.LEFT).get(Genre_.id))
                        );
            }
            if (criteria.getArtistId() != null) {
                specification =
                        specification.and(
                                buildSpecification(criteria.getArtistId(), root -> root.join(Song_.artists, JoinType.LEFT).get(Artist_.id))
                        );
            }
            if (criteria.getInstrumentId() != null) {
                specification =
                        specification.and(
                                buildSpecification(
                                        criteria.getInstrumentId(),
                                        root -> root.join(Song_.instruments, JoinType.LEFT).get(Instrument_.id)
                                )
                        );
            }
        }
        return specification;
    }
}

