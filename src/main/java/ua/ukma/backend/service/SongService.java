package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Song;
import ua.ukma.backend.repository.SongRepository;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class SongService {

    private final SongRepository songRepository;

    public SongService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }
    
    public Song save(Song song) {
        log.debug("Request to save Song : {}", song);
        return songRepository.save(song);
    }
    
    public Optional<Song> partialUpdate(Song song) {
        log.debug("Request to partially update Song : {}", song);

        return songRepository
                .findById(song.getId())
                .map(
                        existingSong -> {
                            if (song.getName() != null) {
                                existingSong.setName(song.getName());
                            }
                            if (song.getLanguage() != null) {
                                existingSong.setLanguage(song.getLanguage());
                            }
                            if (song.getDateAdded() != null) {
                                existingSong.setDateAdded(song.getDateAdded());
                            }
                            if (song.getSpotifyUrl() != null) {
                                existingSong.setSpotifyUrl(song.getSpotifyUrl());
                            }
                            if (song.getYoutubeUrl() != null) {
                                existingSong.setYoutubeUrl(song.getYoutubeUrl());
                            }

                            return existingSong;
                        }
                )
                .map(songRepository::save);
    }
    
    @Transactional(readOnly = true)
    public Page<Song> findAll(Pageable pageable) {
        log.debug("Request to get all Songs");
        return songRepository.findAll(pageable);
    }
    
    @Transactional(readOnly = true)
    public Optional<Song> findOne(Long id) {
        log.debug("Request to get Song : {}", id);
        return songRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Song : {}", id);
        songRepository.deleteById(id);
    }
}