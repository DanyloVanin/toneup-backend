package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Album;
import ua.ukma.backend.repository.AlbumRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class AlbumService {

    private final AlbumRepository albumRepository;

    public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }
    
    public Album save(Album album) {
        log.debug("Request to save Album : {}", album);
        return albumRepository.save(album);
    }
    
    public Optional<Album> partialUpdate(Album album) {
        log.debug("Request to partially update Album : {}", album);

        return albumRepository
                .findById(album.getId())
                .map(
                        existingAlbum -> {
                            if (album.getAlbumName() != null) {
                                existingAlbum.setAlbumName(album.getAlbumName());
                            }
                            if (album.getReleaseYear() != null) {
                                existingAlbum.setReleaseYear(album.getReleaseYear());
                            }
                            if (album.getDescription() != null) {
                                existingAlbum.setDescription(album.getDescription());
                            }
                            if (album.getAlbumPicUrl() != null) {
                                existingAlbum.setAlbumPicUrl(album.getAlbumPicUrl());
                            }

                            return existingAlbum;
                        }
                )
                .map(albumRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Album> findAll() {
        log.debug("Request to get all Albums");
        return albumRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public Optional<Album> findOne(Long id) {
        log.debug("Request to get Album : {}", id);
        return albumRepository.findById(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete Album : {}", id);
        albumRepository.deleteById(id);
    }
}
