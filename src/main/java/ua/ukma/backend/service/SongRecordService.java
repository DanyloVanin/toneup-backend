package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.SongRecord;
import ua.ukma.backend.repository.SongRecordRepository;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class SongRecordService {

    private final SongRecordRepository songRecordRepository;

    public SongRecordService(SongRecordRepository songRecordRepository) {
        this.songRecordRepository = songRecordRepository;
    }
    
    public SongRecord save(SongRecord songRecord) {
        log.debug("Request to save SongRecord : {}", songRecord);
        return songRecordRepository.save(songRecord);
    }

    public Optional<SongRecord> partialUpdate(SongRecord songRecord) {
        log.debug("Request to partially update SongRecord : {}", songRecord);

        return songRecordRepository
                .findById(songRecord.getId())
                .map(
                        existingSongRecord -> {
                            if (songRecord.getTonality() != null) {
                                existingSongRecord.setTonality(songRecord.getTonality());
                            }
                            if (songRecord.getLyrics() != null) {
                                existingSongRecord.setLyrics(songRecord.getLyrics());
                            }
                            if (songRecord.getTempo() != null) {
                                existingSongRecord.setTempo(songRecord.getTempo());
                            }
                            if (songRecord.getRating() != null) {
                                existingSongRecord.setRating(songRecord.getRating());
                            }
                            if (songRecord.getDateUploaded() != null) {
                                existingSongRecord.setDateUploaded(songRecord.getDateUploaded());
                            }
                            if (songRecord.getDifficulty() != null) {
                                existingSongRecord.setDifficulty(songRecord.getDifficulty());
                            }

                            return existingSongRecord;
                        }
                )
                .map(songRecordRepository::save);
    }
    
    @Transactional(readOnly = true)
    public Page<SongRecord> findAll(Pageable pageable) {
        log.debug("Request to get all SongRecords");
        return songRecordRepository.findAll(pageable);
    }
    
    public Page<SongRecord> findAllWithEagerRelationships(Pageable pageable) {
        return songRecordRepository.findAllWithEagerRelationships(pageable);
    }
    
    @Transactional(readOnly = true)
    public Optional<SongRecord> findOne(Long id) {
        log.debug("Request to get SongRecord : {}", id);
        return songRecordRepository.findOneWithEagerRelationships(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete SongRecord : {}", id);
        songRecordRepository.deleteById(id);
    }
}

