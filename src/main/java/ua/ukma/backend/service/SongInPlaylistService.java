package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.SongInPlaylist;
import ua.ukma.backend.repository.SongInPlaylistRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class SongInPlaylistService {

    private final SongInPlaylistRepository songInPlaylistRepository;

    public SongInPlaylistService(SongInPlaylistRepository songInPlaylistRepository) {
        this.songInPlaylistRepository = songInPlaylistRepository;
    }
    
    public SongInPlaylist save(SongInPlaylist songInPlaylist) {
        log.debug("Request to save SongInPlaylist : {}", songInPlaylist);
        return songInPlaylistRepository.save(songInPlaylist);
    }

    
    public Optional<SongInPlaylist> partialUpdate(SongInPlaylist songInPlaylist) {
        log.debug("Request to partially update SongInPlaylist : {}", songInPlaylist);

        return songInPlaylistRepository
                .findById(songInPlaylist.getId())
                .map(
                        existingSongInPlaylist -> {
                            if (songInPlaylist.getDateAdded() != null) {
                                existingSongInPlaylist.setDateAdded(songInPlaylist.getDateAdded());
                            }

                            return existingSongInPlaylist;
                        }
                )
                .map(songInPlaylistRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<SongInPlaylist> findAll() {
        log.debug("Request to get all SongInPlaylists");
        return songInPlaylistRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public Optional<SongInPlaylist> findOne(Long id) {
        log.debug("Request to get SongInPlaylist : {}", id);
        return songInPlaylistRepository.findById(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete SongInPlaylist : {}", id);
        songInPlaylistRepository.deleteById(id);
    }
}
