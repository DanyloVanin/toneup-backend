package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Comment;
import ua.ukma.backend.repository.CommentRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class CommentService {

    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }
    
    public Comment save(Comment comment) {
        log.debug("Request to save Comment : {}", comment);
        return commentRepository.save(comment);
    }
    
    public Optional<Comment> partialUpdate(Comment comment) {
        log.debug("Request to partially update Comment : {}", comment);

        return commentRepository
                .findById(comment.getId())
                .map(
                        existingComment -> {
                            if (comment.getRating() != null) {
                                existingComment.setRating(comment.getRating());
                            }
                            if (comment.getComment() != null) {
                                existingComment.setComment(comment.getComment());
                            }
                            if (comment.getDateCreated() != null) {
                                existingComment.setDateCreated(comment.getDateCreated());
                            }

                            return existingComment;
                        }
                )
                .map(commentRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Comment> findAll() {
        log.debug("Request to get all Comments");
        return commentRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public Optional<Comment> findOne(Long id) {
        log.debug("Request to get Comment : {}", id);
        return commentRepository.findById(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Comment : {}", id);
        commentRepository.deleteById(id);
    }
}

