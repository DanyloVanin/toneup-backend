package ua.ukma.backend.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ukma.backend.domain.User;

@NoArgsConstructor
@Data
public class UserDTO {

    private Long id;
    private String login;

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
    }
    
}
