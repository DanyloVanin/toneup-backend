package ua.ukma.backend.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Chord;
import ua.ukma.backend.repository.ChordRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ChordService {
    
    private final ChordRepository chordRepository;

    public ChordService(ChordRepository chordRepository) {
        this.chordRepository = chordRepository;
    }

    public Chord save(Chord chord) {
        log.debug("Request to save Chord : {}", chord);
        return chordRepository.save(chord);
    }
    
    public Optional<Chord> partialUpdate(Chord chord) {
        log.debug("Request to partially update Chord : {}", chord);

        return chordRepository
                .findById(chord.getId())
                .map(
                        existingChord -> {
                            if (chord.getName() != null) {
                                existingChord.setName(chord.getName());
                            }

                            return existingChord;
                        }
                )
                .map(chordRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Chord> findAll() {
        log.debug("Request to get all Chords");
        return chordRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Chord> findOne(Long id) {
        log.debug("Request to get Chord : {}", id);
        return chordRepository.findById(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete Chord : {}", id);
        chordRepository.deleteById(id);
    }
}

