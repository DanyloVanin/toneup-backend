package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Genre;
import ua.ukma.backend.repository.GenreRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class GenreService {

    private final GenreRepository genreRepository;

    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }
    
    public Genre save(Genre genre) {
        log.debug("Request to save Genre : {}", genre);
        return genreRepository.save(genre);
    }
    
    public Optional<Genre> partialUpdate(Genre genre) {
        log.debug("Request to partially update Genre : {}", genre);

        return genreRepository
                .findById(genre.getId())
                .map(
                        existingGenre -> {
                            if (genre.getName() != null) {
                                existingGenre.setName(genre.getName());
                            }

                            return existingGenre;
                        }
                )
                .map(genreRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Genre> findAll() {
        log.debug("Request to get all Genres");
        return genreRepository.findAllWithEagerRelationships();
    }
    
    public Page<Genre> findAllWithEagerRelationships(Pageable pageable) {
        return genreRepository.findAllWithEagerRelationships(pageable);
    }
    
    @Transactional(readOnly = true)
    public Optional<Genre> findOne(Long id) {
        log.debug("Request to get Genre : {}", id);
        return genreRepository.findOneWithEagerRelationships(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete Genre : {}", id);
        genreRepository.deleteById(id);
    }
}
