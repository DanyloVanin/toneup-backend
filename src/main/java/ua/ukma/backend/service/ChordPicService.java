package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.ChordPic;
import ua.ukma.backend.repository.ChordPicRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ChordPicService {

    private final ChordPicRepository chordPicRepository;

    public ChordPicService(ChordPicRepository chordPicRepository) {
        this.chordPicRepository = chordPicRepository;
    }
    
    public ChordPic save(ChordPic chordPic) {
        log.debug("Request to save ChordPic : {}", chordPic);
        return chordPicRepository.save(chordPic);
    }

    public Optional<ChordPic> partialUpdate(ChordPic chordPic) {
        log.debug("Request to partially update ChordPic : {}", chordPic);

        return chordPicRepository
                .findById(chordPic.getId())
                .map(
                        existingChordPic -> {
                            if (chordPic.getPictureUrl() != null) {
                                existingChordPic.setPictureUrl(chordPic.getPictureUrl());
                            }

                            return existingChordPic;
                        }
                )
                .map(chordPicRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<ChordPic> findAll() {
        log.debug("Request to get all ChordPics");
        return chordPicRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public Optional<ChordPic> findOne(Long id) {
        log.debug("Request to get ChordPic : {}", id);
        return chordPicRepository.findById(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete ChordPic : {}", id);
        chordPicRepository.deleteById(id);
    }
}
