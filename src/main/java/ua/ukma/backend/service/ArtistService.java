package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.Artist;
import ua.ukma.backend.repository.ArtistRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ArtistService {

    private final ArtistRepository artistRepository;

    public ArtistService(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }
    
    public Artist save(Artist artist) {
        log.debug("Request to save Artist : {}", artist);
        return artistRepository.save(artist);
    }

    public Optional<Artist> partialUpdate(Artist artist) {
        log.debug("Request to partially update Artist : {}", artist);

        return artistRepository
                .findById(artist.getId())
                .map(
                        existingArtist -> {
                            if (artist.getArtistName() != null) {
                                existingArtist.setArtistName(artist.getArtistName());
                            }
                            if (artist.getBio() != null) {
                                existingArtist.setBio(artist.getBio());
                            }
                            if (artist.getArtistPicUrl() != null) {
                                existingArtist.setArtistPicUrl(artist.getArtistPicUrl());
                            }

                            return existingArtist;
                        }
                )
                .map(artistRepository::save);
    }
    
    @Transactional(readOnly = true)
    public List<Artist> findAll() {
        log.debug("Request to get all Artists");
        return artistRepository.findAllWithEagerRelationships();
    }
    
    public Page<Artist> findAllWithEagerRelationships(Pageable pageable) {
        return artistRepository.findAllWithEagerRelationships(pageable);
    }
    
    @Transactional(readOnly = true)
    public Optional<Artist> findOne(Long id) {
        log.debug("Request to get Artist : {}", id);
        return artistRepository.findOneWithEagerRelationships(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete Artist : {}", id);
        artistRepository.deleteById(id);
    }
}
