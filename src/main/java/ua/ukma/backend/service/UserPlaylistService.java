package ua.ukma.backend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ukma.backend.domain.UserPlaylist;
import ua.ukma.backend.repository.UserPlaylistRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class UserPlaylistService {

    private final UserPlaylistRepository userPlaylistRepository;

    public UserPlaylistService(UserPlaylistRepository userPlaylistRepository) {
        this.userPlaylistRepository = userPlaylistRepository;
    }
    
    public UserPlaylist save(UserPlaylist userPlaylist) {
        log.debug("Request to save UserPlaylist : {}", userPlaylist);
        return userPlaylistRepository.save(userPlaylist);
    }
    
    public Optional<UserPlaylist> partialUpdate(UserPlaylist userPlaylist) {
        log.debug("Request to partially update UserPlaylist : {}", userPlaylist);

        return userPlaylistRepository
                .findById(userPlaylist.getId())
                .map(
                        existingUserPlaylist -> {
                            if (userPlaylist.getPlaylistName() != null) {
                                existingUserPlaylist.setPlaylistName(userPlaylist.getPlaylistName());
                            }
                            if (userPlaylist.getDateCreated() != null) {
                                existingUserPlaylist.setDateCreated(userPlaylist.getDateCreated());
                            }

                            return existingUserPlaylist;
                        }
                )
                .map(userPlaylistRepository::save);
    }


    @Transactional(readOnly = true)
    public List<UserPlaylist> findAll() {
        log.debug("Request to get all UserPlaylists");
        return userPlaylistRepository.findAll();
    }
    
    @Transactional(readOnly = true)
    public Optional<UserPlaylist> findOne(Long id) {
        log.debug("Request to get UserPlaylist : {}", id);
        return userPlaylistRepository.findById(id);
    }
    
    public void delete(Long id) {
        log.debug("Request to delete UserPlaylist : {}", id);
        userPlaylistRepository.deleteById(id);
    }
}
