package ua.ukma.backend.service.criteria;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ua.ukma.backend.filtering.Criteria;
import ua.ukma.backend.filtering.specific.InstantFilter;
import ua.ukma.backend.filtering.specific.LongFilter;
import ua.ukma.backend.filtering.specific.StringFilter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SongCriteria implements Serializable, Criteria{

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    private StringFilter name;
    private StringFilter language;
    private InstantFilter dateAdded;
    private StringFilter spotifyUrl;
    private StringFilter youtubeUrl;
    private LongFilter songRecordId;
    private LongFilter genreId;
    private LongFilter artistId;
    private LongFilter instrumentId;

    public SongCriteria(SongCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.language = other.language == null ? null : other.language.copy();
        this.dateAdded = other.dateAdded == null ? null : other.dateAdded.copy();
        this.spotifyUrl = other.spotifyUrl == null ? null : other.spotifyUrl.copy();
        this.youtubeUrl = other.youtubeUrl == null ? null : other.youtubeUrl.copy();
        this.songRecordId = other.songRecordId == null ? null : other.songRecordId.copy();
        this.genreId = other.genreId == null ? null : other.genreId.copy();
        this.artistId = other.artistId == null ? null : other.artistId.copy();
        this.instrumentId = other.instrumentId == null ? null : other.instrumentId.copy();
    }

    @Override
    public SongCriteria copy() {
        return new SongCriteria(this);
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public StringFilter language() {
        if (language == null) {
            language = new StringFilter();
        }
        return language;
    }

    public InstantFilter dateAdded() {
        if (dateAdded == null) {
            dateAdded = new InstantFilter();
        }
        return dateAdded;
    }

    public StringFilter spotifyUrl() {
        if (spotifyUrl == null) {
            spotifyUrl = new StringFilter();
        }
        return spotifyUrl;
    }

    public StringFilter youtubeUrl() {
        if (youtubeUrl == null) {
            youtubeUrl = new StringFilter();
        }
        return youtubeUrl;
    }

    public LongFilter songRecordId() {
        if (songRecordId == null) {
            songRecordId = new LongFilter();
        }
        return songRecordId;
    }

    public LongFilter genreId() {
        if (genreId == null) {
            genreId = new LongFilter();
        }
        return genreId;
    }

    public LongFilter artistId() {
        if (artistId == null) {
            artistId = new LongFilter();
        }
        return artistId;
    }

    public LongFilter instrumentId() {
        if (instrumentId == null) {
            instrumentId = new LongFilter();
        }
        return instrumentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SongCriteria that = (SongCriteria) o;
        return (
                Objects.equals(id, that.id) &&
                        Objects.equals(name, that.name) &&
                        Objects.equals(language, that.language) &&
                        Objects.equals(dateAdded, that.dateAdded) &&
                        Objects.equals(spotifyUrl, that.spotifyUrl) &&
                        Objects.equals(youtubeUrl, that.youtubeUrl) &&
                        Objects.equals(songRecordId, that.songRecordId) &&
                        Objects.equals(genreId, that.genreId) &&
                        Objects.equals(artistId, that.artistId) &&
                        Objects.equals(instrumentId, that.instrumentId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, language, dateAdded, spotifyUrl, youtubeUrl, songRecordId, genreId, artistId, instrumentId);
    }
    
}
