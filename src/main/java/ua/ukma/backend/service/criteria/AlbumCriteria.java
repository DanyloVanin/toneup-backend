package ua.ukma.backend.service.criteria;

import lombok.*;
import ua.ukma.backend.filtering.Criteria;
import ua.ukma.backend.filtering.specific.InstantFilter;
import ua.ukma.backend.filtering.specific.LongFilter;
import ua.ukma.backend.filtering.specific.StringFilter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class AlbumCriteria implements Serializable, Criteria{

    private static final long serialVersionUID = 1L;
    
    private LongFilter id;
    private StringFilter albumName;
    private InstantFilter releaseYear;
    private StringFilter description;
    private StringFilter albumPicUrl;
    private LongFilter genreId;
    private LongFilter artistId;

    public AlbumCriteria(AlbumCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.albumName = other.albumName == null ? null : other.albumName.copy();
        this.releaseYear = other.releaseYear == null ? null : other.releaseYear.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.albumPicUrl = other.albumPicUrl == null ? null : other.albumPicUrl.copy();
        this.genreId = other.genreId == null ? null : other.genreId.copy();
        this.artistId = other.artistId == null ? null : other.artistId.copy();
    }

    @Override
    public AlbumCriteria copy() {
        return new AlbumCriteria(this);
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public StringFilter albumName() {
        if (albumName == null) {
            albumName = new StringFilter();
        }
        return albumName;
    }
    
    public InstantFilter releaseYear() {
        if (releaseYear == null) {
            releaseYear = new InstantFilter();
        }
        return releaseYear;
    }
    
    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public StringFilter albumPicUrl() {
        if (albumPicUrl == null) {
            albumPicUrl = new StringFilter();
        }
        return albumPicUrl;
    }
    
    public LongFilter genreId() {
        if (genreId == null) {
            genreId = new LongFilter();
        }
        return genreId;
    }
    
    public LongFilter artistId() {
        if (artistId == null) {
            artistId = new LongFilter();
        }
        return artistId;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AlbumCriteria that = (AlbumCriteria) o;
        return (
                Objects.equals(id, that.id) &&
                        Objects.equals(albumName, that.albumName) &&
                        Objects.equals(releaseYear, that.releaseYear) &&
                        Objects.equals(description, that.description) &&
                        Objects.equals(albumPicUrl, that.albumPicUrl) &&
                        Objects.equals(genreId, that.genreId) &&
                        Objects.equals(artistId, that.artistId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, albumName, releaseYear, description, albumPicUrl, genreId, artistId);
    }
    
}
