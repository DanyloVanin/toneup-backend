package ua.ukma.backend.service.criteria;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ua.ukma.backend.filtering.Criteria;
import ua.ukma.backend.filtering.specific.InstantFilter;
import ua.ukma.backend.filtering.specific.LongFilter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ArticleCriteria implements Serializable, Criteria{

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    private InstantFilter dateCreated;
    private LongFilter authorId;

    public ArticleCriteria(ArticleCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateCreated = other.dateCreated == null ? null : other.dateCreated.copy();
        this.authorId = other.authorId == null ? null : other.authorId.copy();
    }

    @Override
    public ArticleCriteria copy() {
        return new ArticleCriteria(this);
    }
    
    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public InstantFilter dateCreated() {
        if (dateCreated == null) {
            dateCreated = new InstantFilter();
        }
        return dateCreated;
    }

    public LongFilter authorId() {
        if (authorId == null) {
            authorId = new LongFilter();
        }
        return authorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ArticleCriteria that = (ArticleCriteria) o;
        return Objects.equals(id, that.id) && Objects.equals(dateCreated, that.dateCreated) && Objects.equals(authorId, that.authorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateCreated, authorId);
    }

}
