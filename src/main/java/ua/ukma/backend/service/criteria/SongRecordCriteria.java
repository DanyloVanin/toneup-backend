package ua.ukma.backend.service.criteria;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ua.ukma.backend.domain.enumeration.Difficulty;
import ua.ukma.backend.filtering.Criteria;
import ua.ukma.backend.filtering.Filter;
import ua.ukma.backend.filtering.specific.*;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SongRecordCriteria implements Serializable, Criteria{
    
    public static class DifficultyFilter extends Filter<Difficulty>{

        public DifficultyFilter(){
        }

        public DifficultyFilter(DifficultyFilter filter){
            super(filter);
        }

        @Override
        public DifficultyFilter copy(){
            return new DifficultyFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    private StringFilter tonality;
    private IntegerFilter tempo;
    private FloatFilter rating;
    private InstantFilter dateUploaded;
    private DifficultyFilter difficulty;
    private LongFilter authorId;
    private LongFilter songInPlaylistId;
    private LongFilter commentId;
    private LongFilter chordId;
    private LongFilter songId;

    public SongRecordCriteria(SongRecordCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tonality = other.tonality == null ? null : other.tonality.copy();
        this.tempo = other.tempo == null ? null : other.tempo.copy();
        this.rating = other.rating == null ? null : other.rating.copy();
        this.dateUploaded = other.dateUploaded == null ? null : other.dateUploaded.copy();
        this.difficulty = other.difficulty == null ? null : other.difficulty.copy();
        this.authorId = other.authorId == null ? null : other.authorId.copy();
        this.songInPlaylistId = other.songInPlaylistId == null ? null : other.songInPlaylistId.copy();
        this.commentId = other.commentId == null ? null : other.commentId.copy();
        this.chordId = other.chordId == null ? null : other.chordId.copy();
        this.songId = other.songId == null ? null : other.songId.copy();
    }

    @Override
    public SongRecordCriteria copy(){
        return new SongRecordCriteria(this);
    }

    public LongFilter id(){
        if (id == null){
            id = new LongFilter();
        }
        return id;
    }
    
    public StringFilter tonality(){
        if (tonality == null){
            tonality = new StringFilter();
        }
        return tonality;
    }

    public IntegerFilter tempo(){
        if (tempo == null){
            tempo = new IntegerFilter();
        }
        return tempo;
    }
    
    public FloatFilter rating(){
        if (rating == null){
            rating = new FloatFilter();
        }
        return rating;
    }

    public InstantFilter dateUploaded(){
        if (dateUploaded == null){
            dateUploaded = new InstantFilter();
        }
        return dateUploaded;
    }

    public DifficultyFilter difficulty(){
        if (difficulty == null){
            difficulty = new DifficultyFilter();
        }
        return difficulty;
    }

    public LongFilter authorId(){
        if (authorId == null){
            authorId = new LongFilter();
        }
        return authorId;
    }

    public LongFilter songInPlaylistId(){
        if (songInPlaylistId == null){
            songInPlaylistId = new LongFilter();
        }
        return songInPlaylistId;
    }

    public LongFilter commentId(){
        if (commentId == null){
            commentId = new LongFilter();
        }
        return commentId;
    }

    public LongFilter chordId(){
        if (chordId == null){
            chordId = new LongFilter();
        }
        return chordId;
    }

    public LongFilter songId(){
        if (songId == null){
            songId = new LongFilter();
        }
        return songId;
    }

    @Override
    public boolean equals(Object o){
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        final SongRecordCriteria that = (SongRecordCriteria) o;
        return (
                Objects.equals(id, that.id) &&
                        Objects.equals(tonality, that.tonality) &&
                        Objects.equals(tempo, that.tempo) &&
                        Objects.equals(rating, that.rating) &&
                        Objects.equals(dateUploaded, that.dateUploaded) &&
                        Objects.equals(difficulty, that.difficulty) &&
                        Objects.equals(authorId, that.authorId) &&
                        Objects.equals(songInPlaylistId, that.songInPlaylistId) &&
                        Objects.equals(commentId, that.commentId) &&
                        Objects.equals(chordId, that.chordId) &&
                        Objects.equals(songId, that.songId)
        );
    }

    @Override
    public int hashCode(){
        return Objects.hash(
                id,
                tonality,
                tempo,
                rating,
                dateUploaded,
                difficulty,
                authorId,
                songInPlaylistId,
                commentId,
                chordId,
                songId
        );
    }

}
