package ua.ukma.backend.service.criteria;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ua.ukma.backend.filtering.Criteria;
import ua.ukma.backend.filtering.specific.InstantFilter;
import ua.ukma.backend.filtering.specific.LongFilter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class SongInPlaylistCriteria implements Serializable, Criteria{

    private static final long serialVersionUID = 1L;

    private LongFilter id;
    private InstantFilter dateAdded;
    private LongFilter sondRecordId;
    private LongFilter userPlaylistIdId;

    public SongInPlaylistCriteria(SongInPlaylistCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dateAdded = other.dateAdded == null ? null : other.dateAdded.copy();
        this.sondRecordId = other.sondRecordId == null ? null : other.sondRecordId.copy();
        this.userPlaylistIdId = other.userPlaylistIdId == null ? null : other.userPlaylistIdId.copy();
    }

    @Override
    public SongInPlaylistCriteria copy() {
        return new SongInPlaylistCriteria(this);
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public InstantFilter dateAdded() {
        if (dateAdded == null) {
            dateAdded = new InstantFilter();
        }
        return dateAdded;
    }
    
    public LongFilter songRecordId() {
        if (sondRecordId == null) {
            sondRecordId = new LongFilter();
        }
        return sondRecordId;
    }

    public LongFilter userPlaylistId() {
        if (userPlaylistIdId == null) {
            userPlaylistIdId = new LongFilter();
        }
        return userPlaylistIdId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SongInPlaylistCriteria that = (SongInPlaylistCriteria) o;
        return (
                Objects.equals(id, that.id) &&
                        Objects.equals(dateAdded, that.dateAdded) &&
                        Objects.equals(sondRecordId, that.sondRecordId) &&
                        Objects.equals(userPlaylistIdId, that.userPlaylistIdId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateAdded, sondRecordId, userPlaylistIdId);
    }
    
}

