package ua.ukma.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ua.ukma.backend.config.GeneralConfig;

@SpringBootApplication
@EnableConfigurationProperties({ LiquibaseProperties.class, GeneralConfig.class })
public class ToneupBackendApplication {
  
  public static void main(String[] args) {
    SpringApplication.run(ToneupBackendApplication.class, args);
  }

}
