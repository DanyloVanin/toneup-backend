package ua.ukma.backend.config;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

@ConfigurationProperties(prefix = "general")
@Data
public class GeneralConfig{
    
    @Getter
    private Security security;
    private CorsConfiguration cors;
    private Mail mail;
    private Boolean validateMail = false;
    
    @Data
    public static class Mail {
        private String baseUrl;
        private String from;
        private boolean enabled;
    }
    
    @Data
    public static class Security {
        private Authentication authentication;
        private String contentSecurityPolicy;
        
        @Data
        public static class Authentication {
            private Jwt jwt;
            
            @Data
            public static class Jwt{
                private String secret;
                private String base64Secret;
                private long tokenValidityInSeconds;
                private long tokenValidityInSecondsForRememberMe;
            }
        }
    }
    
}
