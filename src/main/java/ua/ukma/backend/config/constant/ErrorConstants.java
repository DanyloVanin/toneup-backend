package ua.ukma.backend.config.constant;

import java.net.URI;

public final class ErrorConstants{
    
    public static final String PROBLEM_BASE_URL = "http://toneup-chords.xyz/problem";
    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");
    public static final URI LOGIN_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/login-already-used");

    private ErrorConstants() {}
}
