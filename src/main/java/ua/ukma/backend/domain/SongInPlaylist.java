package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "song_in_playlist", schema = "public")
@ToString
public class SongInPlaylist implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "date_added", nullable = false)
    @Getter
    @Setter
    private Instant dateAdded;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "author", "songInPlaylists", "comments", "chords", "song" }, allowSetters = true)
    @Getter
    @Setter
    private SongRecord songRecord;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "songPlaylists", "playlistOwner" }, allowSetters = true)
    @Getter
    @Setter
    private UserPlaylist userPlaylist;

    public SongInPlaylist id(Long id) {
        this.id = id;
        return this;
    }

    public SongInPlaylist dateAdded(Instant dateAdded) {
        this.dateAdded = dateAdded;
        return this;
    }

    public SongInPlaylist songRecordId(SongRecord songRecord) {
        this.setSongRecord(songRecord);
        return this;
    }

    public SongInPlaylist userPlaylist(UserPlaylist userPlaylist) {
        this.setUserPlaylist(userPlaylist);
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SongInPlaylist)) {
            return false;
        }
        return id != null && id.equals(((SongInPlaylist) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}