package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "chord_pic", schema = "public")
@ToString
public class ChordPic implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "picture_url", nullable = false)
    @Getter
    @Setter
    private String pictureUrl;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "prevChord", "nextChord", "chordPics", "songRecords" }, allowSetters = true)
    @Getter
    @Setter
    private Chord chord;

    public ChordPic id(Long id) {
        this.id = id;
        return this;
    }

    public ChordPic pictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
        return this;
    }

    public ChordPic chord(Chord chord) {
        this.setChord(chord);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChordPic)) {
            return false;
        }
        return id != null && id.equals(((ChordPic) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

