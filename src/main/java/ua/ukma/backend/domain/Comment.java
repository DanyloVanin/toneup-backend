package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "comment", schema = "public")
@ToString
public class Comment implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "rating", nullable = false)
    @Getter
    @Setter
    @Min(value = 0)
    @Max(value = 5)
    private Float rating;

    @Column(name = "comment")
    @Getter
    @Setter
    private String comment;

    @NotNull
    @Column(name = "date_created", nullable = false)
    @Getter
    @Setter
    private Instant dateCreated;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "userPlaylists", "userComments", "userArticles",
            "email", "firstName", "lastName", "langKey", "resetDate", "sex", "activated", "userSongRecords"
    }, allowSetters = true)
    @Getter
    @Setter
    private User commentAuthor;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "author", "songInPlaylists", "comments", "chords", "song" }, allowSetters = true)
    @Getter
    @Setter
    private SongRecord song;

    public Comment id(Long id) {
        this.id = id;
        return this;
    }

    public Comment rating(Float rating) {
        this.rating = rating;
        return this;
    }

    public Comment comment(String comment) {
        this.comment = comment;
        return this;
    }

    public Comment dateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public Comment commentAuthor(User commentAuthor) {
        this.setCommentAuthor(commentAuthor);
        return this;
    }

    public Comment songId(SongRecord songRecord) {
        this.setSong(songRecord);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Comment)) {
            return false;
        }
        return id != null && id.equals(((Comment) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
}
