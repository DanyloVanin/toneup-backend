package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.BatchSize;
import ua.ukma.backend.config.constant.Constants;
import ua.ukma.backend.domain.enumeration.Sex;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@Entity
@Table(name = "user", schema = "public")
@ToString
public class User extends AbstractAuditingEntity implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex")
    @Getter
    @Setter
    private Sex sex;

    @OneToMany(mappedBy = "playlistOwner")
    @JsonIgnoreProperties(value = { "songPlaylistIds", "playlistOwner" }, allowSetters = true)
    @Getter
    private Set<UserPlaylist> userPlaylists = new HashSet<>();

    @OneToMany(mappedBy = "commentAuthor")
    @JsonIgnoreProperties(value = { "commentAuthor", "songId" }, allowSetters = true)
    @Getter
    private Set<Comment> userComments = new HashSet<>();

    @OneToMany(mappedBy = "articleAuthor")
    @JsonIgnoreProperties(value = { "articleAuthor" }, allowSetters = true)
    @Getter
    private Set<Article> userArticles = new HashSet<>();

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    @Getter
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60, nullable = false)
    @Getter
    @Setter
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    @Getter
    @Setter
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    @Getter
    @Setter
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    @Getter
    @Setter
    private String email;

    @NotNull
    @Column(nullable = false)
    @Getter
    @Setter
    private boolean activated = false;

    @Size(min = 2, max = 10)
    @Column(name = "lang_key", length = 10)
    @Getter
    @Setter
    private String langKey;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    @Getter
    @Setter
    private String imageUrl;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    @Getter
    @Setter
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    @Getter
    @Setter
    private String resetKey;

    @Column(name = "reset_date")
    @Getter
    @Setter
    private Instant resetDate = null;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "user_authority",
            joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "authority_name", referencedColumnName = "name") }
    )
    @BatchSize(size = 20)
    @Getter
    @Setter
    private Set<Authority> authorities = new HashSet<>();

    @OneToMany(mappedBy = "author")
    @JsonIgnoreProperties(value = { "author", "songInPlaylists", "comments", "chords", "song" }, allowSetters = true)
    @Getter
    @Setter
    private Set<SongRecord> userSongRecords = new HashSet<>();
    
    public void setLogin(String login) {
        // login lowercase before saving
        this.login = StringUtils.lowerCase(login, Locale.ENGLISH);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        return id != null && id.equals(((User) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
}
