package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "instrument", schema = "public")
@ToString
public class Instrument implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    @Getter
    @Setter
    private String name;

    @ManyToMany
    @JoinTable(
            name = "rel_instrument__song",
            joinColumns = @JoinColumn(name = "instrument_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id")
    )
    @JsonIgnoreProperties(value = { "songRecords", "genres", "artists", "instruments" }, allowSetters = true)
    @Getter
    @Setter
    private Set<Song> songs = new HashSet<>();

    public Instrument id(Long id) {
        this.id = id;
        return this;
    }

    public Instrument name(String name) {
        this.name = name;
        return this;
    }

    public Instrument songs(Set<Song> songs) {
        this.setSongs(songs);
        return this;
    }

    public Instrument addSong(Song song) {
        this.songs.add(song);
        song.getInstruments().add(this);
        return this;
    }

    public Instrument removeSong(Song song) {
        this.songs.remove(song);
        song.getInstruments().remove(this);
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Instrument)) {
            return false;
        }
        return id != null && id.equals(((Instrument) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
