package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import ua.ukma.backend.domain.enumeration.Difficulty;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "song_record", schema = "public")
@ToString
public class SongRecord implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "tonality", nullable = false)
    @Getter
    @Setter
    private String tonality;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "lyrics", nullable = false)
    @Getter
    @Setter
    private String lyrics;

    @NotNull
    @Column(name = "tempo", nullable = false)
    @Getter
    @Setter
    private Integer tempo;

    @Column(name = "rating")
    @Getter
    @Setter
    @Min(value = 0)
    @Max(value = 5)
    private Float rating;

    @NotNull
    @Column(name = "date_uploaded", nullable = false)
    @Getter
    @Setter
    private Instant dateUploaded;

    @Enumerated(EnumType.STRING)
    @Column(name = "difficulty")
    @Getter
    @Setter
    private Difficulty difficulty;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = { "userPlaylists", "userComments", "userArticles",
            "email", "firstName", "lastName", "langKey", "resetDate", "sex", "userSongRecords"
    }, allowSetters = true)
    @Getter
    @Setter
    private User author;

    @OneToMany(mappedBy = "songRecord")
    @JsonIgnoreProperties(value = { "songRecord", "userPlaylist" }, allowSetters = true)
    @Getter
    private Set<SongInPlaylist> songInPlaylists = new HashSet<>();

    @OneToMany(mappedBy = "song")
    @JsonIgnoreProperties(value = { "commentAuthor", "song" }, allowSetters = true)
    private Set<Comment> comments = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "rel_song_record__chord",
            joinColumns = @JoinColumn(name = "song_record_id"),
            inverseJoinColumns = @JoinColumn(name = "chord_id")
    )
    @JsonIgnoreProperties(value = { "prevChord", "nextChord", "chordPics", "songRecords" }, allowSetters = true)
    @Getter
    @Setter
    private Set<Chord> chords = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "songRecords", "genres", "artists", "instruments" }, allowSetters = true)
    @Getter
    @Setter
    private Song song;
    
    public SongRecord id(Long id) {
        this.id = id;
        return this;
    }

    public SongRecord tonality(String tonality) {
        this.tonality = tonality;
        return this;
    }
    
    public SongRecord lyrics(String lyrics) {
        this.lyrics = lyrics;
        return this;
    }

    public SongRecord tempo(Integer tempo) {
        this.tempo = tempo;
        return this;
    }
    
    public SongRecord rating(Float rating) {
        this.rating = rating;
        return this;
    }

    public SongRecord dateUploaded(Instant dateUploaded) {
        this.dateUploaded = dateUploaded;
        return this;
    }
    
    public SongRecord difficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        return this;
    }

    public SongRecord author(User user) {
        this.setAuthor(user);
        return this;
    }

    public SongRecord songInPlaylists(Set<SongInPlaylist> songInPlaylists) {
        this.setSongInPlaylists(songInPlaylists);
        return this;
    }

    public SongRecord addSongInPlaylist(SongInPlaylist songInPlaylist) {
        this.songInPlaylists.add(songInPlaylist);
        songInPlaylist.setSongRecord(this);
        return this;
    }

    public SongRecord removeSongInPlaylist(SongInPlaylist songInPlaylist) {
        this.songInPlaylists.remove(songInPlaylist);
        songInPlaylist.setSongRecord(null);
        return this;
    }

    public void setSongInPlaylists(Set<SongInPlaylist> songInPlaylists) {
        if (this.songInPlaylists != null) {
            this.songInPlaylists.forEach(i -> i.setSongRecord(null));
        }
        if (songInPlaylists != null) {
            songInPlaylists.forEach(i -> i.setSongRecord(this));
        }
        this.songInPlaylists = songInPlaylists;
    }

    public Set<Comment> getComments() {
        return this.comments;
    }

    public SongRecord comments(Set<Comment> comments) {
        this.setComments(comments);
        return this;
    }

    public SongRecord addComment(Comment comment) {
        this.comments.add(comment);
        comment.setSong(this);
        return this;
    }

    public SongRecord removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setSong(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        if (this.comments != null) {
            this.comments.forEach(i -> i.setSong(null));
        }
        if (comments != null) {
            comments.forEach(i -> i.setSong(this));
        }
        this.comments = comments;
    }
    
    public SongRecord chords(Set<Chord> chords) {
        this.setChords(chords);
        return this;
    }

    public SongRecord addChord(Chord chord) {
        this.chords.add(chord);
        chord.getSongRecords().add(this);
        return this;
    }

    public SongRecord removeChord(Chord chord) {
        this.chords.remove(chord);
        chord.getSongRecords().remove(this);
        return this;
    }
    
    public SongRecord song(Song song) {
        this.setSong(song);
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SongRecord)) {
            return false;
        }
        return id != null && id.equals(((SongRecord) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
