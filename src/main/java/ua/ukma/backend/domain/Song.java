package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "song", schema = "public")
@ToString
public class Song implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    @Getter
    @Setter
    private String name;

    @Column(name = "language")
    @Getter
    @Setter
    private String language;

    @Column(name = "date_added")
    @Getter
    @Setter
    private Instant dateAdded;

    @Column(name = "spotify_url")
    @Getter
    @Setter
    private String spotifyUrl;

    @Column(name = "youtube_url")
    @Getter
    @Setter
    private String youtubeUrl;

    @OneToMany(mappedBy = "song")
    @JsonIgnoreProperties(value = { "author", "songInPlaylists", "comments", "chords", "song" }, allowSetters = true)
    @Getter
    private Set<SongRecord> songRecords = new HashSet<>();

    @ManyToMany(mappedBy = "songs")
    @JsonIgnoreProperties(value = { "songs", "albums" }, allowSetters = true)
    @Getter
    private Set<Genre> genres = new HashSet<>();

    @ManyToMany(mappedBy = "songs")
    @JsonIgnoreProperties(value = { "albums", "songs" }, allowSetters = true)
    @Getter
    private Set<Artist> artists = new HashSet<>();

    @ManyToMany(mappedBy = "songs")
    @JsonIgnoreProperties(value = { "chordPics", "songs" }, allowSetters = true)
    @Getter
    private Set<Instrument> instruments = new HashSet<>();

    public Song id(Long id) {
        this.id = id;
        return this;
    }

    public Song name(String name) {
        this.name = name;
        return this;
    }
    
    public Song language(String language) {
        this.language = language;
        return this;
    }

    public Song dateAdded(Instant dateAdded) {
        this.dateAdded = dateAdded;
        return this;
    }

    public Song spotifyUrl(String spotifyUrl) {
        this.spotifyUrl = spotifyUrl;
        return this;
    }

    public Song youtubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
        return this;
    }

    public Song songRecords(Set<SongRecord> songRecords) {
        this.setSongRecords(songRecords);
        return this;
    }

    public Song addSongRecord(SongRecord songRecord) {
        this.songRecords.add(songRecord);
        songRecord.setSong(this);
        return this;
    }

    public Song removeSongRecord(SongRecord songRecord) {
        this.songRecords.remove(songRecord);
        songRecord.setSong(null);
        return this;
    }

    public void setSongRecords(Set<SongRecord> songRecords) {
        if (this.songRecords != null) {
            this.songRecords.forEach(i -> i.setSong(null));
        }
        if (songRecords != null) {
            songRecords.forEach(i -> i.setSong(this));
        }
        this.songRecords = songRecords;
    }

    public Song genres(Set<Genre> genres) {
        this.setGenres(genres);
        return this;
    }

    public Song addGenre(Genre genre) {
        this.genres.add(genre);
        genre.getSongs().add(this);
        return this;
    }

    public Song removeGenre(Genre genre) {
        this.genres.remove(genre);
        genre.getSongs().remove(this);
        return this;
    }

    public void setGenres(Set<Genre> genres) {
        if (this.genres != null) {
            this.genres.forEach(i -> i.removeSong(this));
        }
        if (genres != null) {
            genres.forEach(i -> i.addSong(this));
        }
        this.genres = genres;
    }

    public Song artistIds(Set<Artist> artists) {
        this.setArtistIds(artists);
        return this;
    }

    public Song addArtistId(Artist artist) {
        this.artists.add(artist);
        artist.getSongs().add(this);
        return this;
    }

    public Song removeArtistId(Artist artist) {
        this.artists.remove(artist);
        artist.getSongs().remove(this);
        return this;
    }

    public void setArtistIds(Set<Artist> artists) {
        if (this.artists != null) {
            this.artists.forEach(i -> i.removeSong(this));
        }
        if (artists != null) {
            artists.forEach(i -> i.addSong(this));
        }
        this.artists = artists;
    }

    public Song instrumentIds(Set<Instrument> instruments) {
        this.setInstrumentIds(instruments);
        return this;
    }

    public Song addInstrumentId(Instrument instrument) {
        this.instruments.add(instrument);
        instrument.getSongs().add(this);
        return this;
    }

    public Song removeInstrumentId(Instrument instrument) {
        this.instruments.remove(instrument);
        instrument.getSongs().remove(this);
        return this;
    }

    public void setInstrumentIds(Set<Instrument> instruments) {
        if (this.instruments != null) {
            this.instruments.forEach(i -> i.removeSong(this));
        }
        if (instruments != null) {
            instruments.forEach(i -> i.addSong(this));
        }
        this.instruments = instruments;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Song)) {
            return false;
        }
        return id != null && id.equals(((Song) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
