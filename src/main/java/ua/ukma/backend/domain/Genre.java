package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "genre", schema = "public")
@ToString
public class Genre implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    @Getter
    @Setter
    private String name;

    @ManyToMany
    @JoinTable(
            name = "rel_genre__song",
            joinColumns = @JoinColumn(name = "genre_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id")
    )
    @JsonIgnoreProperties(value = { "songRecords", "genres", "artists", "instruments" }, allowSetters = true)
    @Getter
    @Setter
    private Set<Song> songs = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "rel_genre__album",
            joinColumns = @JoinColumn(name = "genre_id"),
            inverseJoinColumns = @JoinColumn(name = "album_id")
    )
    @JsonIgnoreProperties(value = { "genres", "artists" }, allowSetters = true)
    @Getter
    @Setter
    private Set<Album> albums = new HashSet<>();

    public Genre id(Long id) {
        this.id = id;
        return this;
    }

    public Genre name(String name) {
        this.name = name;
        return this;
    }

    public Genre songs(Set<Song> songs) {
        this.setSongs(songs);
        return this;
    }

    public Genre addSong(Song song) {
        this.songs.add(song);
        song.getGenres().add(this);
        return this;
    }

    public Genre removeSong(Song song) {
        this.songs.remove(song);
        song.getGenres().remove(this);
        return this;
    }
    
    public Genre albums(Set<Album> albums) {
        this.setAlbums(albums);
        return this;
    }

    public Genre addAlbum(Album album) {
        this.albums.add(album);
        album.getGenres().add(this);
        return this;
    }

    public Genre removeAlbum(Album album) {
        this.albums.remove(album);
        album.getGenres().remove(this);
        return this;
    }

     @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Genre)) {
            return false;
        }
        return id != null && id.equals(((Genre) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
}
