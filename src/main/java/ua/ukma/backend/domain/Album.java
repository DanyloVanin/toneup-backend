package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "album", schema = "public")
@ToString
public class Album implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "album_name", nullable = false)
    @Getter
    @Setter
    private String albumName;

    @Column(name = "release_year")
    @Getter
    @Setter
    private Instant releaseYear;

    @Column(name = "description")
    @Getter
    @Setter
    private String description;

    @Column(name = "album_pic_url")
    @Getter
    @Setter
    private String albumPicUrl;

    @ManyToMany(mappedBy = "albums")
    @JsonIgnoreProperties(value = { "songs", "albums" }, allowSetters = true)
    @Getter
    private Set<Genre> genres = new HashSet<>();

    @ManyToMany(mappedBy = "albums")
    @JsonIgnoreProperties(value = { "albums", "songs" }, allowSetters = true)
    @Getter
    private Set<Artist> artists = new HashSet<>();

    public Album id(Long id) {
        this.id = id;
        return this;
    }

    public Album albumName(String albumName) {
        this.albumName = albumName;
        return this;
    }

    public Album releaseYear(Instant releaseYear) {
        this.releaseYear = releaseYear;
        return this;
    }

    public Album description(String description) {
        this.description = description;
        return this;
    }

    public Album albumPicUrl(String albumPicUrl) {
        this.albumPicUrl = albumPicUrl;
        return this;
    }

    public Album genres(Set<Genre> genres) {
        this.setGenres(genres);
        return this;
    }

    public Album addGenre(Genre genre) {
        this.genres.add(genre);
        genre.getAlbums().add(this);
        return this;
    }

    public Album removeGenre(Genre genre) {
        this.genres.remove(genre);
        genre.getAlbums().remove(this);
        return this;
    }

    public void setGenres(Set<Genre> genres) {
        if (this.genres != null) {
            this.genres.forEach(i -> i.removeAlbum(this));
        }
        if (genres != null) {
            genres.forEach(i -> i.addAlbum(this));
        }
        this.genres = genres;
    }

    public Album artists(Set<Artist> artists) {
        this.setArtists(artists);
        return this;
    }

    public Album addArtist(Artist artist) {
        this.artists.add(artist);
        artist.getAlbums().add(this);
        return this;
    }

    public Album removeArtist(Artist artist) {
        this.artists.remove(artist);
        artist.getAlbums().remove(this);
        return this;
    }

    public void setArtists(Set<Artist> artists) {
        if (this.artists != null) {
            this.artists.forEach(i -> i.removeAlbum(this));
        }
        if (artists != null) {
            artists.forEach(i -> i.addAlbum(this));
        }
        this.artists = artists;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Album)) {
            return false;
        }
        return id != null && id.equals(((Album) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
