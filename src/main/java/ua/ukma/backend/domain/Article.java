package ua.ukma.backend.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "article", schema = "public")
@ToString
public class Article implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "text", nullable = false)
    @Getter
    @Setter
    private String text;

    @NotNull
    @Column(name = "date_created", nullable = false)
    @Getter
    @Setter
    private Instant dateCreated = Instant.now();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "userPlaylists", "userComments", "userArticles",
    "email", "firstName", "lastName", "langKey", "resetDate", "sex", "userSongRecords"
    }, allowSetters = true)
    @Getter
    @Setter
    private User articleAuthor;

    public Article id(Long id) {
        this.id = id;
        return this;
    }

    public Article text(String text) {
        this.text = text;
        return this;
    }

    public Article dateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }
    
    public Article articleAuthor(User articleAuthor) {
        this.setArticleAuthor(articleAuthor);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Article)) {
            return false;
        }
        return id != null && id.equals(((Article) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

