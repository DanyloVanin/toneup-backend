package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "chord", schema = "public")
@ToString
public class Chord implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    @Getter
    @Setter
    private String name;

    @JsonIgnoreProperties(value = { "prevChord", "nextChord", "chordPics", "songRecords" }, allowSetters = true)
    @OneToOne
    @Getter
    @Setter
    @JsonIgnore
    private Chord prevChord;

    @JsonIgnoreProperties(value = { "prevChord", "nextChord", "chordPics", "songRecords" }, allowSetters = true)
    @OneToOne
    @Getter
    @Setter
    @JsonIgnore
    private Chord nextChord;

    @OneToMany(mappedBy = "chord")
    @JsonIgnoreProperties(value = { "chord", "instrument" }, allowSetters = true)
    @Getter
    private Set<ChordPic> chordPics = new HashSet<>();

    @ManyToMany(mappedBy = "chords")
    @JsonIgnoreProperties(value = { "author", "songInPlaylists", "comments", "chords", "song" }, allowSetters = true)
    @Getter
    private Set<SongRecord> songRecords = new HashSet<>();
    

    public Chord id(Long id) {
        this.id = id;
        return this;
    }

    public Chord name(String name) {
        this.name = name;
        return this;
    }

    public Chord prevChord(Chord chord) {
        this.setPrevChord(chord);
        return this;
    }

    public Chord nextChord(Chord chord) {
        this.setNextChord(chord);
        return this;
    }

    public Chord chordPics(Set<ChordPic> chordPics) {
        this.setChordPics(chordPics);
        return this;
    }

    public Chord addChordPic(ChordPic chordPic) {
        this.chordPics.add(chordPic);
        chordPic.setChord(this);
        return this;
    }

    public Chord removeChordPic(ChordPic chordPic) {
        this.chordPics.remove(chordPic);
        chordPic.setChord(null);
        return this;
    }

    public void setChordPics(Set<ChordPic> chordPics) {
        if (this.chordPics != null) {
            this.chordPics.forEach(i -> i.setChord(null));
        }
        if (chordPics != null) {
            chordPics.forEach(i -> i.setChord(this));
        }
        this.chordPics = chordPics;
    }

    public Chord songRecordIds(Set<SongRecord> songRecords) {
        this.setSongRecordIds(songRecords);
        return this;
    }

    public Chord addSongRecordId(SongRecord songRecord) {
        this.songRecords.add(songRecord);
        songRecord.getChords().add(this);
        return this;
    }

    public Chord removeSongRecordId(SongRecord songRecord) {
        this.songRecords.remove(songRecord);
        songRecord.getChords().remove(this);
        return this;
    }

    public void setSongRecordIds(Set<SongRecord> songRecords) {
        if (this.songRecords != null) {
            this.songRecords.forEach(i -> i.removeChord(this));
        }
        if (songRecords != null) {
            songRecords.forEach(i -> i.addChord(this));
        }
        this.songRecords = songRecords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Chord)) {
            return false;
        }
        return id != null && id.equals(((Chord) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}