package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "artist", schema = "public")
@ToString
public class Artist implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "artist_name", nullable = false)
    @Getter
    @Setter
    private String artistName;

    @Column(name = "bio")
    @Getter
    @Setter
    private String bio;

    @Column(name = "artist_pic_url")
    @Getter
    @Setter
    private String artistPicUrl;

    @ManyToMany
    @JoinTable(
            name = "rel_artist__album",
            joinColumns = @JoinColumn(name = "artist_id"),
            inverseJoinColumns = @JoinColumn(name = "album_id")
    )
    @JsonIgnoreProperties(value = { "genres", "artists" }, allowSetters = true)
    @Getter
    @Setter
    private Set<Album> albums = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "rel_artist__song",
            joinColumns = @JoinColumn(name = "artist_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id")
    )
    @JsonIgnoreProperties(value = { "songRecords", "genres", "artists", "instruments" }, 
            allowSetters = true)
    @Getter
    @Setter
    private Set<Song> songs = new HashSet<>();

    public Artist id(Long id) {
        this.id = id;
        return this;
    }

    public Artist artistName(String artistName) {
        this.artistName = artistName;
        return this;
    }

    public Artist bio(String bio) {
        this.bio = bio;
        return this;
    }

    public Artist artistPicUrl(String artistPicUrl) {
        this.artistPicUrl = artistPicUrl;
        return this;
    }

    public Artist albums(Set<Album> albums) {
        this.setAlbums(albums);
        return this;
    }

    public Artist addAlbum(Album album) {
        this.albums.add(album);
        album.getArtists().add(this);
        return this;
    }

    public Artist removeAlbum(Album album) {
        this.albums.remove(album);
        album.getArtists().remove(this);
        return this;
    }

    public Artist songs(Set<Song> songs) {
        this.setSongs(songs);
        return this;
    }

    public Artist addSong(Song song) {
        this.songs.add(song);
        song.getArtists().add(this);
        return this;
    }

    public Artist removeSong(Song song) {
        this.songs.remove(song);
        song.getArtists().remove(this);
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Artist)) {
            return false;
        }
        return id != null && id.equals(((Artist) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
}

