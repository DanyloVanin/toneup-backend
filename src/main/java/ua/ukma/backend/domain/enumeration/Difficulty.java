package ua.ukma.backend.domain.enumeration;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD,
}
