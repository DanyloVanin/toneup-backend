package ua.ukma.backend.domain.enumeration;

public enum Sex {
    MALE,
    FEMALE,
    OTHER,
}