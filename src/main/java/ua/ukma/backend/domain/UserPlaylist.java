package ua.ukma.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user_playlist", schema = "public")
@ToString
public class UserPlaylist implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Getter
    @Setter
    private Long id;

    @NotNull
    @Column(name = "playlist_name", nullable = false)
    @Getter
    @Setter
    private String playlistName;

    @NotNull
    @Column(name = "date_created", nullable = false)
    @Getter
    @Setter
    private Instant dateCreated;

    @OneToMany(mappedBy = "userPlaylist")
    @JsonIgnoreProperties(value = { "songRecord", "userPlaylist" }, allowSetters = true)
    @Getter
    private Set<SongInPlaylist> songPlaylists = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "userPlaylists", "userComments", "userArticles",
            "email", "firstName", "lastName", "langKey", "resetDate", "sex", "userSongRecords"
    }, allowSetters = true)
    @Getter
    @Setter
    private User playlistOwner;

    public UserPlaylist id(Long id) {
        this.id = id;
        return this;
    }

    public UserPlaylist playlistName(String playlistName) {
        this.playlistName = playlistName;
        return this;
    }

    public UserPlaylist dateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public UserPlaylist songPlaylists(Set<SongInPlaylist> songInPlaylists) {
        this.setSongPlaylists(songInPlaylists);
        return this;
    }

    public UserPlaylist addSongPlaylist(SongInPlaylist songInPlaylist) {
        this.songPlaylists.add(songInPlaylist);
        songInPlaylist.setUserPlaylist(this);
        return this;
    }

    public UserPlaylist removeSongPlaylist(SongInPlaylist songInPlaylist) {
        this.songPlaylists.remove(songInPlaylist);
        songInPlaylist.setUserPlaylist(null);
        return this;
    }

    public void setSongPlaylists(Set<SongInPlaylist> songInPlaylists) {
        if (this.songPlaylists != null) {
            this.songPlaylists.forEach(i -> i.setUserPlaylist(null));
        }
        if (songInPlaylists != null) {
            songInPlaylists.forEach(i -> i.setUserPlaylist(this));
        }
        this.songPlaylists = songInPlaylists;
    }

    public UserPlaylist playlistOwner(User playlistOwner) {
        this.setPlaylistOwner(playlistOwner);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPlaylist)) {
            return false;
        }
        return id != null && id.equals(((UserPlaylist) o).id);
    }

    @Override
    public int hashCode() {
       return getClass().hashCode();
    }
    
}

