package ua.ukma.backend.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Artist;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long>{
    @Query(
            value = "select distinct artist from Artist artist left join fetch artist.albums left join fetch artist.songs",
            countQuery = "select count(distinct artist) from Artist artist"
    )
    Page<Artist> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct artist from Artist artist left join fetch artist.albums left join fetch artist.songs")
    List<Artist> findAllWithEagerRelationships();

    @Query("select artist from Artist artist left join fetch artist.albums left join fetch artist.songs where artist.id =:id")
    Optional<Artist> findOneWithEagerRelationships(@Param("id") Long id);
}

