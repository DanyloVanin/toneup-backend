package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.ukma.backend.domain.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, String>{}

