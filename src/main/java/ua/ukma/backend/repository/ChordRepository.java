package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Chord;

@Repository
public interface ChordRepository extends JpaRepository<Chord, Long>{}
