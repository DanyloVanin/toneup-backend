package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{}

