package ua.ukma.backend.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.SongRecord;

import java.util.List;
import java.util.Optional;

@Repository
public interface SongRecordRepository extends JpaRepository<SongRecord, Long>, JpaSpecificationExecutor<SongRecord>{
    @Query(
            value = "select distinct songRecord from SongRecord songRecord left join fetch songRecord.chords",
            countQuery = "select count(distinct songRecord) from SongRecord songRecord"
    )
    Page<SongRecord> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct songRecord from SongRecord songRecord left join fetch songRecord.chords")
    List<SongRecord> findAllWithEagerRelationships();

    @Query("select songRecord from SongRecord songRecord left join fetch songRecord.chords where songRecord.id =:id")
    Optional<SongRecord> findOneWithEagerRelationships(@Param("id") Long id);
}
