package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long>, 
        JpaSpecificationExecutor<Article>{}

