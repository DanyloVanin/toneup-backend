package ua.ukma.backend.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Instrument;

import java.util.List;
import java.util.Optional;


@Repository
public interface InstrumentRepository extends JpaRepository<Instrument, Long>{
    @Query(
            value = "select distinct instrument from Instrument instrument left join fetch instrument.songs",
            countQuery = "select count(distinct instrument) from Instrument instrument"
    )
    Page<Instrument> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct instrument from Instrument instrument left join fetch instrument.songs")
    List<Instrument> findAllWithEagerRelationships();

    @Query("select instrument from Instrument instrument left join fetch instrument.songs where instrument.id =:id")
    Optional<Instrument> findOneWithEagerRelationships(@Param("id") Long id);
}

