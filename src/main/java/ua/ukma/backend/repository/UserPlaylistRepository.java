package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.UserPlaylist;

@Repository
public interface UserPlaylistRepository extends JpaRepository<UserPlaylist, Long>{}

