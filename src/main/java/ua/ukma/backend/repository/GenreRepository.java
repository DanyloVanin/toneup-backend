package ua.ukma.backend.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.Genre;

import java.util.List;
import java.util.Optional;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{
    @Query(
            value = "select distinct genre from Genre genre left join fetch genre.songs left join fetch genre.albums",
            countQuery = "select count(distinct genre) from Genre genre"
    )
    Page<Genre> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct genre from Genre genre left join fetch genre.songs left join fetch genre.albums")
    List<Genre> findAllWithEagerRelationships();

    @Query("select genre from Genre genre left join fetch genre.songs left join fetch genre.albums where genre.id =:id")
    Optional<Genre> findOneWithEagerRelationships(@Param("id") Long id);
}