package ua.ukma.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ukma.backend.domain.ChordPic;

@Repository
public interface ChordPicRepository extends JpaRepository<ChordPic, Long>{}