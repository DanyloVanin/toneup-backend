package ua.ukma.backend.filtering;

public interface Criteria {
    Criteria copy();
}