package ua.ukma.backend.filtering.specific;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ua.ukma.backend.filtering.Filter;

import java.util.Objects;

@NoArgsConstructor
public class StringFilter extends Filter<String>{
    private static final long serialVersionUID = 1L;

    @Getter
    private String contains;
    @Getter
    private String doesNotContain;

    public StringFilter(StringFilter filter){
        super(filter);
        this.contains = filter.contains;
        this.doesNotContain = filter.doesNotContain;
    }

    public StringFilter copy(){
        return new StringFilter(this);
    }

    public StringFilter setContains(String contains){
        this.contains = contains;
        return this;
    }

    public StringFilter setDoesNotContain(String doesNotContain){
        this.doesNotContain = doesNotContain;
        return this;
    }

    public boolean equals(Object o){
        if (this == o){
            return true;
        } else if (o != null && this.getClass() == o.getClass()){
            if (!super.equals(o)){
                return false;
            } else{
                StringFilter that = (StringFilter) o;
                return Objects.equals(this.contains, that.contains) && Objects.equals(this.doesNotContain, that.doesNotContain);
            }
        } else{
            return false;
        }
    }

    public int hashCode(){
        return Objects.hash(super.hashCode(), this.contains, this.doesNotContain);
    }

    public String toString(){
        return this.getFilterName() + " [" + (this.getEquals() != null ? "equals=" + (String) this.getEquals() + ", " : "") + (this.getNotEquals() != null ? "notEquals=" + (String) this.getNotEquals() + ", " : "") + (this.getSpecified() != null ? "specified=" + this.getSpecified() + ", " : "") + (this.getIn() != null ? "in=" + this.getIn() + ", " : "") + (this.getNotIn() != null ? "notIn=" + this.getNotIn() + ", " : "") + (this.getContains() != null ? "contains=" + this.getContains() + ", " : "") + (this.getDoesNotContain() != null ? "doesNotContain=" + this.getDoesNotContain() : "") + "]";
    }
}
