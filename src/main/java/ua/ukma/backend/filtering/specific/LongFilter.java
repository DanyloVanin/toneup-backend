package ua.ukma.backend.filtering.specific;

import lombok.NoArgsConstructor;
import ua.ukma.backend.filtering.RangeFilter;

@NoArgsConstructor
public class LongFilter extends RangeFilter<Long>{
    private static final long serialVersionUID = 1L;

    public LongFilter(LongFilter filter){
        super(filter);
    }

    public LongFilter copy(){
        return new LongFilter(this);
    }
}

