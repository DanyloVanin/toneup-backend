package ua.ukma.backend.filtering.specific;

import lombok.NoArgsConstructor;
import ua.ukma.backend.filtering.RangeFilter;

@NoArgsConstructor
public class FloatFilter extends RangeFilter<Float>{
    private static final long serialVersionUID = 1L;

    public FloatFilter(FloatFilter filter) {
        super(filter);
    }

    public FloatFilter copy() {
        return new FloatFilter(this);
    }
}
