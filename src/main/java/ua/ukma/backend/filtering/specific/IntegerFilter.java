package ua.ukma.backend.filtering.specific;

import lombok.NoArgsConstructor;
import ua.ukma.backend.filtering.RangeFilter;

@NoArgsConstructor
public class IntegerFilter extends RangeFilter<Integer>{
    private static final long serialVersionUID = 1L;

    public IntegerFilter(IntegerFilter filter){
        super(filter);
    }

    public IntegerFilter copy(){
        return new IntegerFilter(this);
    }
}
